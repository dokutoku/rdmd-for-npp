//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
/**
 * plugin command list
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module rdmd_for_npp.menu_commands;


version (Windows):

//pragma(lib, "Comdlg32");
//pragma(lib, "Shell32");

private static import core.sys.windows.commdlg;
private static import core.sys.windows.shellapi;
private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;
private static import std.file;
private static import std.path;
private static import std.string;
private static import std.traits;
private static import std.utf;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private static import npp_api.PowerEditor.menuCmdID;
private static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
private static import npp_api.pluginfunc.scintilla_msg;
private static import npp_api.pluginfunc.npp_msgs;
private static import npp_api.pluginfunc.lang;
private static import npp_api.pluginfunc.menu;
private static import npp_api.pluginfunc.path;
private static import rdmd_for_npp.auto_action;
private static import rdmd_for_npp.console;
private static import rdmd_for_npp.dlang_option;
private static import rdmd_for_npp.msg;
private static import rdmd_for_npp.plugin_dll;
private static import rdmd_for_npp.project;
private static import rdmd_for_npp.internal_action;

extern (C)
extern
npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.NppData nppData;

pure nothrow @safe
private wstring to_lower_internal(const wchar[] ext)

	do
	{
		static import std.ascii;

		wchar[] output = new wchar[ext.length];

		for (size_t i = 0; i < output.length; i++) {
			output[i] = std.ascii.toLower(ext[i]);
		}

		return output.idup;
	}

/**
 * plugin command
 */
extern (C)
nothrow
void create_hello()

	in
	{
	}

	do
	{
		static import core.sys.windows.windef;
		static import core.sys.windows.winnt;
		static import std.file;
		static import std.path;
		static import std.string;
		static import std.utf;
		static import npp_api.PowerEditor.menuCmdID;
		static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
		static import npp_api.pluginfunc.npp_msgs;
		static import npp_api.pluginfunc.lang;
		static import npp_api.pluginfunc.scintilla_msg;
		static import npp_api.pluginfunc.string;
		static import rdmd_for_npp.auto_action;
		static import rdmd_for_npp.plugin_dll;

		npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType lang_type = npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_EXTERNAL;
		string hello_text = null;
		wstring hello_filename = null;
		wstring lang_ext = null;
		core.sys.windows.winnt.WCHAR[npp_api.pluginfunc.path.OS_MAX_PATH] input_buf = '\0';

		//get lang type
		npp_api.pluginfunc.npp_msgs.send_NPPM_GETCURRENTLANGTYPE(.nppData._nppHandle, lang_type);

		//get file name
		if (npp_api.pluginfunc.npp_msgs.send_NPPM_GETFILENAME(.nppData._nppHandle, npp_api.pluginfunc.path.OS_MAX_PATH, &(input_buf[0]))) {
			//get lang ext
			lang_ext = to_lower_internal(std.path.extension(npp_api.pluginfunc.string.from_stringz(input_buf)));
		}

		if (lang_ext != null) {
			hello_filename = std.path.setExtension(`hello`w, lang_ext);
		} else {
			lang_ext = npp_api.pluginfunc.lang.lang_to_ext!(wstring)(lang_type);

			if (lang_ext != null) {
				hello_filename = std.path.setExtension(`hello`w , lang_ext);
			}
		}

		if (hello_filename.length != 0) {
			npp_api.pluginfunc.npp_msgs.send_NPPM_GETPLUGINSCONFIGDIR(.nppData._nppHandle, npp_api.pluginfunc.path.OS_MAX_PATH, &(input_buf[0]));
			wstring npp_config_path = npp_api.pluginfunc.string.from_stringz(input_buf);

			try {
				wstring hello_file = std.path.buildPath(npp_config_path, `rdmd\hello\`w, hello_filename);

				if (std.file.exists(hello_file) && std.file.isFile(hello_file)) {
					hello_text = cast(string)(std.file.read(hello_file));
				} else {
					hello_text = rdmd_for_npp.auto_action.load_default_p_text(lang_ext);
				}
			} catch (Exception e) {
				rdmd_for_npp.plugin_dll.msgbox(e.msg);
			}
		}

		hello_text ~= '\0';

		//New file
		npp_api.pluginfunc.npp_msgs.send_NPPM_MENUCOMMAND(.nppData._nppHandle, npp_api.PowerEditor.menuCmdID.IDM_FILE_NEW);

		//Force UTF-8
		try {
			std.utf.validate(hello_text);
			npp_api.pluginfunc.npp_msgs.send_NPPM_MENUCOMMAND(.nppData._nppHandle, npp_api.PowerEditor.menuCmdID.IDM_FORMAT_CONV2_AS_UTF_8);
		} catch (Exception e) {
		}

		//Force lang style
		if (npp_api.pluginfunc.lang.is_known_lang(lang_type)) {
			npp_api.pluginfunc.npp_msgs.send_NPPM_SETCURRENTLANGTYPE(.nppData._nppHandle, lang_type);
		}

		int currentEdit = -1;
		npp_api.pluginfunc.npp_msgs.send_NPPM_GETCURRENTSCINTILLA(.nppData._nppHandle, currentEdit);

		if (currentEdit == -1) {
			return;
		}

		if (currentEdit == 0) {
			npp_api.pluginfunc.scintilla_msg.send_SCI_SETTEXT(.nppData._scintillaMainHandle, &(hello_text[0]));
		} else {
			npp_api.pluginfunc.scintilla_msg.send_SCI_SETTEXT(.nppData._scintillaSecondHandle, &(hello_text[0]));
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow
void open_hello()

	in
	{
	}

	do
	{
		static import core.sys.windows.shellapi;
		static import core.sys.windows.windef;
		static import core.sys.windows.winuser;
		static import std.file;
		static import std.path;
		static import std.utf;
		static import npp_api.pluginfunc.string;
		static import rdmd_for_npp.plugin_dll;

		try {
			core.sys.windows.winnt.WCHAR[npp_api.pluginfunc.path.OS_MAX_PATH] input_buf = '\0';

			if (npp_api.pluginfunc.npp_msgs.send_NPPM_GETPLUGINSCONFIGDIR(.nppData._nppHandle, input_buf.length, &(input_buf[0]))) {
				string hello_dir = std.utf.toUTF8(std.path.buildNormalizedPath(npp_api.pluginfunc.string.from_stringz(input_buf), `rdmd/hello/`w));

				if (!std.file.exists(hello_dir) || !std.file.isDir(hello_dir)) {
					std.file.mkdirRecurse(hello_dir);

					if (!std.file.exists(hello_dir) || !std.file.isDir(hello_dir)) {
						rdmd_for_npp.plugin_dll.trusted_msgbox(npp_api.pluginfunc.string.c_wstring!(`Unknown hello directory`w));

						return;
					}
				}

				core.sys.windows.shellapi.ShellExecuteW(.nppData._nppHandle, &(npp_api.pluginfunc.string.c_wstring!(`open`w)[0]), std.utf.toUTF16z(hello_dir), core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, core.sys.windows.winuser.SW_SHOWNORMAL);
			}
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow
void dmd_run()

	do
	{
		static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
		static import npp_api.pluginfunc.lang;
		static import rdmd_for_npp.auto_action;
		static import rdmd_for_npp.dlang_option;

		try {
			rdmd_for_npp.auto_action.dlang_action(rdmd_for_npp.dlang_option.dlang_compiler.dmd);
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow
void ldc2_run()

	do
	{
		static import npp_api.pluginfunc.path;
		static import rdmd_for_npp.auto_action;
		static import rdmd_for_npp.dlang_option;

		try {
			rdmd_for_npp.auto_action.dlang_action(rdmd_for_npp.dlang_option.dlang_compiler.ldc2);
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}

/+
/**
 * plugin command
 */
extern (C)
nothrow
void gdc_run()

	do
	{
		static import npp_api.pluginfunc.path;
		static import rdmd_for_npp.auto_action;
		static import rdmd_for_npp.dlang_option;

		try {

		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}
+/

/**
 * plugin command
 */
extern (C)
nothrow
void dub_run()

	do
	{
		static import std.path;
		static import npp_api.pluginfunc.lang;
		static import rdmd_for_npp.auto_action;
		static import rdmd_for_npp.msg;
		static import rdmd_for_npp.plugin_dll;

		try {
			wstring dub_path;

			if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`compiler_ldc2`)].func_item._init2Check) {
				wstring ldc2_path = rdmd_for_npp.auto_action.ldc2_path();

				if (std.path.isAbsolute(ldc2_path)) {
					dub_path = std.path.buildPath(std.path.dirName(ldc2_path), `dub.exe`w).idup;
				} else {
					//Unknown LDC dub.exe PATH
					dub_path = `dub.exe`w;
				}
			} else {
				dub_path = npp_api.pluginfunc.lang.lang_path(`.dub`w);
			}

			rdmd_for_npp.auto_action.dub_action(dub_path, null);
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow
void search_dub()

	do
	{
		static import std.path;
		static import npp_api.pluginfunc.path;
		static import rdmd_for_npp.auto_action;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.project;

		wstring path = rdmd_for_npp.auto_action.search_make(npp_api.pluginfunc.path.get_file_path(.nppData._nppHandle), [`dub.json`w, `dub.sdl`w]);

		if (path != null) {
			rdmd_for_npp.console.r_console.cd(std.path.dirName(path));
			rdmd_for_npp.project.current_project.load(path);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow
void open_project()

	do
	{
		static import core.sys.windows.commdlg;
		static import core.sys.windows.windef;
		static import core.sys.windows.winnt;
		static import core.sys.windows.winuser;
		static import std.path;
		static import std.string;
		static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
		static import npp_api.pluginfunc.string;
		static import rdmd_for_npp.internal_action;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.project;

		core.sys.windows.winnt.WCHAR[npp_api.pluginfunc.path.OS_MAX_PATH] input_buf = '\0';
		wstring initial_dir = null;

		try {
			if (rdmd_for_npp.project.current_project.project_path != null) {
				initial_dir = rdmd_for_npp.internal_action.create_init_dir(rdmd_for_npp.project.current_project.project_directory).idup;
			} else if (rdmd_for_npp.console.r_console.current_path.absolute_path != null) {
				initial_dir = rdmd_for_npp.internal_action.create_init_dir(rdmd_for_npp.console.r_console.current_path.absolute_path).idup;
			}
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}

		if (initial_dir == null) {
			initial_dir = npp_api.pluginfunc.string.c_wstring!(`\\?\%HOMEDRIVE%%HOMEPATH%`w).idup;
		} else {
			initial_dir = std.path.buildPath(`\\?\`w, initial_dir, "\0"w).idup;
		}

		core.sys.windows.commdlg.OPENFILENAMEW open_file =
		{
			lStructSize: core.sys.windows.commdlg.OPENFILENAMEW.sizeof,
			hwndOwner: .nppData._nppHandle,
			hInstance: core.sys.windows.windef.NULL,
			lpstrFilter: &("D Project file(*.json *.sdl)\0*.json;*.sdl\0All file(*.*)\0*.*\0\0"w[0]),
			lpstrCustomFilter: core.sys.windows.windef.NULL,
			nMaxCustFilter: 0,
			nFilterIndex: 1,
			lpstrFile: &(input_buf[0]),
			nMaxFile: npp_api.pluginfunc.path.OS_MAX_PATH,
			lpstrFileTitle: core.sys.windows.windef.NULL,
			nMaxFileTitle: 0,
			lpstrInitialDir: &(initial_dir[0]),
			lpstrTitle: core.sys.windows.windef.NULL,
			Flags: core.sys.windows.commdlg.OFN_EXPLORER|core.sys.windows.commdlg.OFN_FILEMUSTEXIST,
			nFileOffset: 0,
			nFileExtension: 0,
			lpstrDefExt: core.sys.windows.windef.NULL,
			lCustData: 0,
			lpfnHook: core.sys.windows.windef.NULL,
			lpTemplateName: core.sys.windows.windef.NULL,
			//lpEditInfo: core.sys.windows.windef.NULL,
			//lpstrPrompt: core.sys.windows.windef.NULL,
			pvReserved: core.sys.windows.windef.NULL,
			dwReserved: 0,
			FlagsEx: 0,
		};

		if (core.sys.windows.commdlg.GetOpenFileNameW(&open_file) != 0) {
			rdmd_for_npp.project.current_project.load(input_buf);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow @nogc
void close_project()

	do
	{
		static import rdmd_for_npp.project;
		static import rdmd_for_npp.auto_menu_commands;

		rdmd_for_npp.project.current_project.close();
	}

/**
 * plugin command
 */
extern (C)
nothrow
void cd_file_directory()

	do
	{
		static import std.path;
		static import npp_api.pluginfunc.path;
		static import rdmd_for_npp.console;

		try {
			wstring directory_path = npp_api.pluginfunc.path.get_directory_path(.nppData._nppHandle);

			if (directory_path != null) {
				rdmd_for_npp.console.r_console.cd(directory_path);
			}
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow
void cd_project_directory()

	do
	{
		static import std.path;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.project;

		try {
			if (rdmd_for_npp.project.current_project.project_directory != null) {
				rdmd_for_npp.console.r_console.cd(rdmd_for_npp.project.current_project.project_directory);
			}
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow @nogc
void open_file_directory()

	do
	{
		static import core.sys.windows.shellapi;
		static import core.sys.windows.windef;
		static import core.sys.windows.winnt;
		static import core.sys.windows.winuser;
		static import npp_api.pluginfunc.npp_msgs;
		static import npp_api.pluginfunc.string;

		core.sys.windows.winnt.WCHAR[npp_api.pluginfunc.path.OS_MAX_PATH] input_buf;

		if (npp_api.pluginfunc.npp_msgs.send_NPPM_GETCURRENTDIRECTORY(.nppData._nppHandle, npp_api.pluginfunc.path.OS_MAX_PATH, &(input_buf[0])) == core.sys.windows.windef.TRUE) {
			core.sys.windows.shellapi.ShellExecuteW(.nppData._nppHandle, &(npp_api.pluginfunc.string.c_wstring!(`open`w)[0]), &(input_buf[0]), core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, core.sys.windows.winuser.SW_SHOWNORMAL);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow
void open_project_directory()

	do
	{
		static import core.sys.windows.shellapi;
		static import core.sys.windows.windef;
		static import core.sys.windows.winuser;
		static import std.utf;
		static import npp_api.pluginfunc.string;
		static import rdmd_for_npp.project;

		try {
			if (rdmd_for_npp.project.current_project.project_directory != null) {
				core.sys.windows.shellapi.ShellExecuteW(.nppData._nppHandle, &(npp_api.pluginfunc.string.c_wstring!(`open`w)[0]), std.utf.toUTF16z(std.path.buildPath(`\\?\`w, rdmd_for_npp.project.current_project.project_directory)), core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, core.sys.windows.winuser.SW_SHOWNORMAL);
			}
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow
void auto_run()

	do
	{
		static import core.sys.windows.windef;
		static import core.sys.windows.winuser;
		static import std.file;
		static import npp_api.PowerEditor.menuCmdID;
		static import npp_api.pluginfunc.npp_msgs;
		static import rdmd_for_npp.auto_action;

		try {
			//Save
			npp_api.pluginfunc.npp_msgs.send_NPPM_MENUCOMMAND(.nppData._nppHandle, npp_api.PowerEditor.menuCmdID.IDM_FILE_SAVE);

			wstring absolute_path = npp_api.pluginfunc.path.get_file_path(.nppData._nppHandle);

			if ((absolute_path.length == 0) || (!std.file.exists(absolute_path) || !std.file.isFile(absolute_path))) {
				return;
			}

			npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType lang_type;
			npp_api.pluginfunc.npp_msgs.send_NPPM_GETCURRENTLANGTYPE(.nppData._nppHandle, lang_type);

			rdmd_for_npp.auto_action.auto_action(absolute_path, lang_type);
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow
void open_console()

	do
	{
		static import rdmd_for_npp.console;

		if (rdmd_for_npp.console.r_console.is_opened()) {
			rdmd_for_npp.console.r_console.restore_console();

			return;
		}

		rdmd_for_npp.console.r_console.new_console();
	}

/**
 * plugin command
 */
extern (C)
nothrow @nogc
void close_console()

	do
	{
		static import rdmd_for_npp.console;

		rdmd_for_npp.console.r_console.close();
	}

/**
 * plugin command sub 1
 */
extern (C)
nothrow
void cmd_console()

	do
	{
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.plugin_dll;
		static import npp_api.pluginfunc.path;
		static import npp_api.pluginfunc.string;
		static import rdmd_for_npp.auto_menu_commands;

		wchar[] path;
		wstring temp = npp_api.pluginfunc.path.echo_variables(`ComSpec`w);

		if (temp != null) {
			path = (temp ~ '\0').dup;
		} else {
			path = npp_api.pluginfunc.string.c_wstring!(`cmd.exe`w).dup;
		}

		rdmd_for_npp.console.r_console.new_console(&(path[0]));
		rdmd_for_npp.auto_menu_commands.check_cmd_console.auto_change_check();
	}

/**
 * plugin command sub 2
 */
extern (C)
nothrow
void powershell_console()

	do
	{
		static import rdmd_for_npp.console;
		static import npp_api.pluginfunc.path;
		static import npp_api.pluginfunc.string;
		static import rdmd_for_npp.auto_menu_commands;

		wchar[] path = (npp_api.pluginfunc.path.search_exe(`powershell.exe`w) ~ '\0').dup;
		rdmd_for_npp.console.r_console.new_console(&(path[0]));
		rdmd_for_npp.auto_menu_commands.check_powershell_console.auto_change_check();
	}

/**
 * plugin command sub 3
 */
extern (C)
nothrow
void change_console()

	do
	{
		static import core.sys.windows.commdlg;
		static import core.sys.windows.windef;
		static import core.sys.windows.winnt;
		static import std.string;
		static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
		static import npp_api.pluginfunc.string;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.plugin_dll;
		static import rdmd_for_npp.auto_menu_commands;

		core.sys.windows.winnt.WCHAR[npp_api.pluginfunc.path.OS_MAX_PATH] input_buf = '\0';
		core.sys.windows.winnt.WCHAR[npp_api.pluginfunc.path.OS_MAX_PATH] output_buf = '\0';

		core.sys.windows.commdlg.OPENFILENAMEW open_file =
		{
			lStructSize: core.sys.windows.commdlg.OPENFILENAMEW.sizeof,
			hwndOwner: .nppData._nppHandle,
			hInstance: core.sys.windows.windef.NULL,
			lpstrFilter: &("exe file(*.exe)\0*.exe\0All file(*.*)\0*.*\0\0"w[0]),
			lpstrCustomFilter: core.sys.windows.windef.NULL,
			nMaxCustFilter: 0,
			nFilterIndex: 1,
			lpstrFile: &(input_buf[0]),
			nMaxFile: npp_api.pluginfunc.path.OS_MAX_PATH,
			lpstrFileTitle: core.sys.windows.windef.NULL,
			nMaxFileTitle: 0,
			lpstrInitialDir: &(npp_api.pluginfunc.string.c_wstring!(`\\?\%SystemDrive%`w)[0]),
			lpstrTitle: core.sys.windows.windef.NULL,
			Flags: core.sys.windows.commdlg.OFN_EXPLORER|core.sys.windows.commdlg.OFN_FILEMUSTEXIST,
			nFileOffset: 0,
			nFileExtension: 0,
			lpstrDefExt: core.sys.windows.windef.NULL,
			lCustData: 0,
			lpfnHook: core.sys.windows.windef.NULL,
			lpTemplateName: core.sys.windows.windef.NULL,
			//lpEditInfo: core.sys.windows.windef.NULL,
			//lpstrPrompt: core.sys.windows.windef.NULL,
			pvReserved: core.sys.windows.windef.NULL,
			dwReserved: 0,
			FlagsEx: 0,
		};

		try {
			if (core.sys.windows.commdlg.GetOpenFileNameW(&open_file) != 0) {
				if (rdmd_for_npp.console.r_console.is_opened()) {
					rdmd_for_npp.console.r_console.close();
				}

				wchar[] path = npp_api.pluginfunc.string.to_cstring(input_buf, output_buf).dup;
				rdmd_for_npp.console.r_console.new_console(&(path[0]));

				//ToDo:
				version (none) {
					rdmd_for_npp.auto_menu_commands.check_other_file_console.auto_change_check();
				}
			}
		} catch (Exception e) {
			//ToDo: error
			rdmd_for_npp.plugin_dll.msgbox(e.msg);
		}
	}

/**
 * plugin command
 */
extern (C)
nothrow @nogc
void open_config_folder()

	do
	{
		static import core.sys.windows.shellapi;
		static import core.sys.windows.windef;
		static import core.sys.windows.winnt;
		static import core.sys.windows.winuser;
		static import npp_api.pluginfunc.npp_msgs;
		static import npp_api.pluginfunc.string;

		core.sys.windows.winnt.WCHAR[npp_api.pluginfunc.path.OS_MAX_PATH] input_buf = '\0';
		npp_api.pluginfunc.npp_msgs.send_NPPM_GETPLUGINSCONFIGDIR(.nppData._nppHandle, input_buf.length, &(input_buf[0]));
		core.sys.windows.shellapi.ShellExecuteW(.nppData._nppHandle, &(npp_api.pluginfunc.string.c_wstring!(`open`w)[0]), &(input_buf[0]), core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, core.sys.windows.winuser.SW_SHOWNORMAL);
	}
