//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
/**
 * dlang option control
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module rdmd_for_npp.dlang_option;


private static import core.sys.windows.winuser;
private static import npp_api.pluginfunc.basic_interface;
private static import npp_api.pluginfunc.lang;
private static import npp_api.pluginfunc.menu;
private static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private static import rdmd_for_npp.auto_action;
private static import rdmd_for_npp.dlang_option;
private static import rdmd_for_npp.plugin_dll;
private static import std.path;

extern (C)
extern
npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.NppData nppData;

core.sys.windows.winuser.MENUITEMINFOW change_menu_check =
{
	cbSize: core.sys.windows.winuser.MENUITEMINFOW.sizeof,
	fMask: core.sys.windows.winuser.MIIM_STATE,
	fState: core.sys.windows.winuser.MFS_ENABLED,
};

enum dlang_compiler
{
	none,
	dmd,
	ldc2,
	gdc,
}

/**
 * LDC2のクロスコンパイリングをチェックする
 *
 * Params:
 *      check_ldc2 = LDCコンパイラであるかを確認するかどうか
 */
nothrow
bool is_ldc2_windows_cross_compiling()

	do
	{
		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`compiler_ldc2`)].func_item._init2Check && npp_api.pluginfunc.menu.is_chid_menu_checked(rdmd_for_npp.plugin_dll.menu_index, rdmd_for_npp.plugin_dll.search_index!(`ldc2 cross compiling`))) {
			if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x32_Windows`)].func_item._init2Check) {
				return true;
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64_Windows`)].func_item._init2Check) {
				return true;
			}
		}

		return false;
	}

pure nothrow @safe
private void add_option(ref wstring output_option, const wchar[] input_option)

	do
	{
		if (output_option.length != 0) {
			output_option ~= ` `w;
			output_option ~= input_option;
		} else {
			output_option ~= (input_option).idup;
		}
	}

nothrow
wstring dmd_option(const wchar[] input_option)

	in
	{
	}

	do
	{
		static import rdmd_for_npp.plugin_dll;

		wstring output_option;

		if (input_option.length != 0) {
			output_option ~= input_option.idup;
		}

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x32`)].func_item._init2Check) {
			if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_mscoff`)].func_item._init2Check) {
				.add_option(output_option, `-m32mscoff`w);
			} else {
				.add_option(output_option, `-m32`w);
			}
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64`)].func_item._init2Check) {
			.add_option(output_option, `-m64`w);
		}

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_betterC`)].func_item._init2Check) {
			.add_option(output_option, `-betterC`w);
		}

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_plain`)].func_item._init2Check) {

		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_debug`)].func_item._init2Check) {
			.add_option(output_option, `-debug -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_release`)].func_item._init2Check) {
			.add_option(output_option, `-release -O -inline`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_release_debug`)].func_item._init2Check) {
			.add_option(output_option, `-release -O -inline -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_release_nobounds`)].func_item._init2Check) {
			.add_option(output_option, `-release -O -inline -boundscheck=off`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_unittest`)].func_item._init2Check) {
			.add_option(output_option, `-unittest -debug -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_docs`)].func_item._init2Check) {
			.add_option(output_option, `-o- -c -Dddocs`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_ddox`)].func_item._init2Check) {
			.add_option(output_option, `-o- -c -Df__dummy.html -Xfdocs.json`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_profile`)].func_item._init2Check) {
			.add_option(output_option, `-profile -O -inline -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_profile_gc`)].func_item._init2Check) {
			.add_option(output_option, `-profile=gc -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_cov`)].func_item._init2Check) {
			.add_option(output_option, `-cov -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_unittest_cov`)].func_item._init2Check) {
			.add_option(output_option, `-unittest -cov -debug -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_syntax`)].func_item._init2Check) {
			.add_option(output_option, `-o-`w);
		}

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_main`)].func_item._init2Check) {
			.add_option(output_option, `-main`w);
		}

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_rdmd`)].func_item._init2Check) {
			if (!rdmd_for_npp.auto_action.is_absolute_dmd_rdmd_path()) {
				.add_option(output_option, `-run`w);
			}
		}

		return output_option.idup;
	}

nothrow
wstring ldc_option(const wchar[] input_option)

	in
	{
	}

	do
	{
		static import rdmd_for_npp.plugin_dll;
		static import npp_api.pluginfunc.menu;

		wstring output_option;

		if (input_option.length != 0) {
			output_option ~= input_option.idup;
		}

		if (npp_api.pluginfunc.menu.is_chid_menu_checked(rdmd_for_npp.plugin_dll.menu_index, rdmd_for_npp.plugin_dll.search_index!(`ldc2 cross compiling`))) {
			if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x32_Windows`)].func_item._init2Check) {
				.add_option(output_option, `--mtriple=i686-windows-msvc`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64_Windows`)].func_item._init2Check) {
				.add_option(output_option, `--mtriple=x86_64-windows-msvc`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64_Linux`)].func_item._init2Check) {
				.add_option(output_option, `--mtriple=x86_64-linux-gnu`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64_MacOS`)].func_item._init2Check) {
				.add_option(output_option, `--mtriple=x86_64-apple-darwin16.7.0`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`aarch64`)].func_item._init2Check) {
				.add_option(output_option, `--mtriple=aarch64-linux-gnu`w);
			/+
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Android_aarch64`)].func_item._init2Check) {
				.add_option(output_option, `--mtriple=`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Android_armv7a`)].func_item._init2Check) {
				.add_option(output_option, `--mtriple=`w);
			+/
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`wasm32`)].func_item._init2Check) {
				.add_option(output_option, `--mtriple=wasm32-unknown-unknown-webassembly --betterC -L-export-dynamic`w);
			}
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x32`)].func_item._init2Check) {
			.add_option(output_option, `--m32`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64`)].func_item._init2Check) {
			.add_option(output_option, `--m64`w);
		}

		//not wasm
		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_betterC`)].func_item._init2Check && !rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`wasm32`)].func_item._init2Check) {
			.add_option(output_option, `--betterC`w);
		}

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_plain`)].func_item._init2Check) {

		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_debug`)].func_item._init2Check) {
			.add_option(output_option, `--d-debug -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_release`)].func_item._init2Check) {
			.add_option(output_option, `--release --O2`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_release_debug`)].func_item._init2Check) {
			.add_option(output_option, `--release --O2 -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_release_nobounds`)].func_item._init2Check) {
			.add_option(output_option, `--release --O2 --boundscheck=off`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_unittest`)].func_item._init2Check) {
			.add_option(output_option, `--unittest --d-debug -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_docs`)].func_item._init2Check) {
			.add_option(output_option, `--o- -c --Dd=docs`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_ddox`)].func_item._init2Check) {
			.add_option(output_option, `--o- -c --Df=__dummy.html --Xf=docs.json`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_profile`)].func_item._init2Check) {
			//ToDo: --profile?
			//.add_option(output_option, `--profile --O2 -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_profile_gc`)].func_item._init2Check) {
			//ToDo: --profile?
			//.add_option(output_option, `--profile=gc -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_cov`)].func_item._init2Check) {
			.add_option(output_option, `--cov -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_unittest_cov`)].func_item._init2Check) {
			.add_option(output_option, `--unittest --cov --d-debug -g`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_syntax`)].func_item._init2Check) {
			.add_option(output_option, `--o-`w);
		}

		//not wasm
		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_main`)].func_item._init2Check && !rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`wasm32`)].func_item._init2Check) {
			.add_option(output_option, `--main`w);
		}

		//not other OS
		if ((rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_rdmd`)].func_item._init2Check) && !(npp_api.pluginfunc.menu.is_chid_menu_checked(rdmd_for_npp.plugin_dll.menu_index, rdmd_for_npp.plugin_dll.search_index!(`ldc2 cross compiling`)) && !is_ldc2_windows_cross_compiling())) {
			if (!rdmd_for_npp.auto_action.is_absolute_ldc2_rdmd_path()) {
				.add_option(output_option, `--run`w);
			}
		}

		return output_option.idup;
	}

/+
nothrow
wstring gdc_option(const wchar[] input_option)


	do
	{
	}
+/

nothrow
wstring dub_option(const wchar[] input_option)

	in
	{
	}

	do
	{
		static import rdmd_for_npp.plugin_dll;

		wstring output_option;

		if ((rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_run`)].func_item._init2Check) && !(rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`compiler_ldc2`)].func_item._init2Check && npp_api.pluginfunc.menu.is_chid_menu_checked(rdmd_for_npp.plugin_dll.menu_index, rdmd_for_npp.plugin_dll.search_index!(`ldc2 cross compiling`)) && !rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x32_Windows`)].func_item._init2Check && !rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64_Windows`)].func_item._init2Check)) {
			.add_option(output_option, `run`w);
		} else {
			.add_option(output_option, `build`w);
		}

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`compiler_ldc2`)].func_item._init2Check && npp_api.pluginfunc.menu.is_chid_menu_checked(rdmd_for_npp.plugin_dll.menu_index, rdmd_for_npp.plugin_dll.search_index!(`ldc2 cross compiling`))) {
			if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x32_Windows`)].func_item._init2Check) {
				.add_option(output_option, `--arch=i686-windows-msvc`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64_Windows`)].func_item._init2Check) {
				.add_option(output_option, `--arch=x86_64-windows-msvc`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64_Linux`)].func_item._init2Check) {
				.add_option(output_option, `--arch=x86_64-linux-gnu`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64_MacOS`)].func_item._init2Check) {
				.add_option(output_option, `--arch=x86_64-apple-darwin16.7.0`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`aarch64`)].func_item._init2Check) {
				.add_option(output_option, `--arch=aarch64-linux-gnu`w);
			/+
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Android_aarch64`)].func_item._init2Check) {
				.add_option(output_option, `--arch=`w);
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Android_armv7a`)].func_item._init2Check) {
				.add_option(output_option, `--arch=`w);
			+/
			} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`wasm32`)].func_item._init2Check) {
				.add_option(output_option, `--arch=wasm32-unknown-unknown-webassembly`w);
			}
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x32`)].func_item._init2Check) {
			//mscoff and not ldc2 compiler
			if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_mscoff`)].func_item._init2Check && !rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`compiler_ldc2`)].func_item._init2Check) {
				.add_option(output_option, `--arch=x86_mscoff`w);
			} else {
				.add_option(output_option, `--arch=x86`w);
			}
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64`)].func_item._init2Check) {
			.add_option(output_option, `--arch=x86_64`w);
		}

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`compiler_dmd`)].func_item._init2Check) {
			.add_option(output_option, `--compiler=`w ~ rdmd_for_npp.auto_action.dmd_path());
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`compiler_ldc2`)].func_item._init2Check) {
			.add_option(output_option, `--compiler=`w ~ rdmd_for_npp.auto_action.ldc2_path());
		}

		/+
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`compiler_gdc`)].func_item._init2Check) {
			.add_option(output_option, `--compiler=`w ~ );
		}
		+/

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_force`)].func_item._init2Check) {
			.add_option(output_option, `--force`w);
		}

		if (input_option.length != 0) {
			.add_option(output_option, input_option);
		}

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_plain`)].func_item._init2Check) {
			.add_option(output_option, `--build=plain`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_debug`)].func_item._init2Check) {
			.add_option(output_option, `--build=debug`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_release`)].func_item._init2Check) {
			.add_option(output_option, `--build=release`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_release_debug`)].func_item._init2Check) {
			.add_option(output_option, `--build=release-debug`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_release_nobounds`)].func_item._init2Check) {
			.add_option(output_option, `--build=release-nobounds`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_unittest`)].func_item._init2Check) {
			.add_option(output_option, `--build=unittest`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_docs`)].func_item._init2Check) {
			.add_option(output_option, `--build=docs`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_ddox`)].func_item._init2Check) {
			.add_option(output_option, `--build=ddox`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_profile`)].func_item._init2Check) {
			.add_option(output_option, `--build=profile`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_profile_gc`)].func_item._init2Check) {
			.add_option(output_option, `--build=profile-gc`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_cov`)].func_item._init2Check) {
			.add_option(output_option, `--build=cov`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_unittest_cov`)].func_item._init2Check) {
			.add_option(output_option, `--build=unittest-cov`w);
		} else if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`build_syntax`)].func_item._init2Check) {
			.add_option(output_option, `--build=syntax`w);
		}

		return output_option.idup;
	}
