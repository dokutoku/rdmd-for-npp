//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
/**
 * project
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module rdmd_for_npp.project;


version (Windows):

private static import core.sys.windows.winuser;
private static import std.file;
private static import std.path;
private static import std.string;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private static import rdmd_for_npp.plugin_dll;
private static import npp_api.pluginfunc.menu;
private static import npp_api.pluginfunc.basic_interface;
private static import npp_api.pluginfunc.path;
private static import rdmd_for_npp.auto_menu_commands;

extern (C)
extern
npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.NppData nppData;

pragma(inline, true)
nothrow @nogc
void open_check()

	do
	{
		npp_api.pluginfunc.menu.enable_check(.nppData._nppHandle, rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Open project`)].func_item);
		npp_api.pluginfunc.menu.disable_check(.nppData._nppHandle, rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Close project`)].func_item);
	}

pragma(inline, true)
nothrow @nogc
void close_check()

	do
	{
		npp_api.pluginfunc.menu.enable_check(.nppData._nppHandle, rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Close project`)].func_item);
		npp_api.pluginfunc.menu.disable_check(.nppData._nppHandle, rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Open project`)].func_item);
	}

struct project_info
{
	static import core.sys.windows.winuser;
	static import std.file;
	static import std.path;
	static import std.string;
	static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
	static import rdmd_for_npp.plugin_dll;
	static import npp_api.pluginfunc.string;
	static import rdmd_for_npp.auto_menu_commands;

	wstring file_name = null;
	wstring project_path = null;
	wstring project_directory = null;

	nothrow
	void load(ref wchar[npp_api.pluginfunc.path.OS_MAX_PATH] input_path)

		do
		{
			wstring path_temp = npp_api.pluginfunc.string.from_stringz(input_path);

			try {
				if (!std.file.exists(path_temp) || !std.file.isFile(path_temp)) {
					rdmd_for_npp.plugin_dll.trusted_msgbox(npp_api.pluginfunc.string.c_wstring!(`Unknown project file path`w));
					this.close();

					return;
				}
			} catch (Exception e) {
				this.close();

				return;
			}

			this.file_name = std.path.baseName(path_temp).idup;
			this.project_directory = std.path.dirName(path_temp).idup;
			this.project_path = path_temp.idup;
			open_check();
		}

	nothrow
	void load(const ref wchar[] input_path)

		do
		{
			try {
				if (!std.file.exists(input_path) || !std.file.isFile(input_path)) {
					rdmd_for_npp.plugin_dll.trusted_msgbox(npp_api.pluginfunc.string.c_wstring!(`Unknown project file path`w));
					this.close();

					return;
				}
			} catch (Exception e) {
				this.close();

				return;
			}

			this.file_name = std.path.baseName(input_path).idup;
			this.project_directory = std.path.dirName(input_path).idup;
			this.project_path = input_path.idup;
			open_check();
		}

	nothrow @nogc
	void close()

		do
		{
			this.file_name = null;
			this.project_directory = null;
			this.project_path = null;
			close_check();
		}
}

.project_info current_project;
