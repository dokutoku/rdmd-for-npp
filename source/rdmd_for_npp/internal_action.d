//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
/**
 * common internal action
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module rdmd_for_npp.internal_action;


private static import std.traits;
private static import std.file;
private static import std.path;

const (C)[] create_init_dir(C)(const (C)[] input)
	if (std.traits.isSomeChar!(C))

	do
	{
		static import std.file;
		static import std.path;

		if ((input.length == 0) || !std.path.isValidPath(input)) {
			return null;
		}

		if (std.file.exists(input) && std.file.isDir(input)) {
			return input;
		}

		const (C)[] current_path = input;

		foreach_reverse (path_temp; std.path.pathSplitter(input)) {
			if ((path_temp.length == 1) && std.path.isDirSeparator(path_temp[0])) {
				continue;
			}

			current_path = std.path.dirName(current_path);

			if (std.file.exists(current_path) && std.file.isDir(current_path)) {
				return current_path;
			}
		}

		if (std.file.exists(current_path) && std.file.isDir(current_path)) {
			return current_path;
		}

		return null;
	}
