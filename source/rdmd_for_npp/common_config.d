/**
 * common plugin config
 */
module rdmd_for_npp.common_config;


private static import std.format;
private static import std.utf;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private static import npp_api.pluginfunc.config_file;
private static import npp_api.pluginfunc.string;

enum name = `RDMD for Notepad++`;
enum UTF16_name = std.utf.toUTF16(`RDMD for Notepad++`).idup;
enum version_string = `0.1.0.2`;
enum author = `dokutoku`;
enum about_msg = npp_api.pluginfunc.string.c_wstring!(std.format.format!("Version: %s\r\n\r\nLicense: GPL\r\n\r\nAuthor %s"w)(.version_string, .author)).idup;

version (X86_64) {
	enum is_X86_64 = true;
	enum is_X86 = false;
} else {
	enum is_X86_64 = false;
	enum is_X86 = true;
}

enum npp_api.pluginfunc.config_file.plugin_config_info plugin_config =
{
	type: npp_api.pluginfunc.config_file.config_type_t.ini,
	directory_name: `rdmd`,
	file_name: `rdmd`,
	ini_section_name: `rdmd_for_npp`,
	settings:
	[
		{
			name: `enable_startup_console`,
			type: npp_api.pluginfunc.config_file.value_type.bool_type,
			//value: {bool_value: false},
		},
		{
			name: `first_message`,
			type: npp_api.pluginfunc.config_file.value_type.bool_type,
		},
		{
			name: `previous_x`,
			type: npp_api.pluginfunc.config_file.value_type.int_type,
		},
		{
			name: `previous_y`,
			type: npp_api.pluginfunc.config_file.value_type.int_type,
		},
		{
			name: `previous_width`,
			type: npp_api.pluginfunc.config_file.value_type.int_type,
		},
		{
			name: `previous_height`,
			type: npp_api.pluginfunc.config_file.value_type.int_type,
		},
		{
			name: `console_path`,
			type: npp_api.pluginfunc.config_file.value_type.path_type,
			//value: {string_value: `test`},
		},
		{
			name: `project_file`,
			type: npp_api.pluginfunc.config_file.value_type.path_type,
		},
		{
			name: `current_directory`,
			type: npp_api.pluginfunc.config_file.value_type.path_type,
		},
	],
};

/**
 * Alt + N
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey create_hello_key =
{
	_isAlt: true,
	_isCtrl: false,
	_isShift: false,
	_key: 'N',
};

/**
 * Alt + L
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey ldc_run_key =
{
	_isAlt: true,
	_isCtrl: false,
	_isShift: false,
	_key: 'L',
};

/**
 * Alt + D
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey dub_run_key =
{
	_isAlt: true,
	_isCtrl: false,
	_isShift: false,
	_key: 'D',
};

/**
 * Alt + C
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey search_dub_key =
{
	_isAlt: true,
	_isCtrl: false,
	_isShift: false,
	_key: 'C',
};

/**
 * Alt + Ctrl + C
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey cd_file_directory_key =
{
	_isAlt: true,
	_isCtrl: true,
	_isShift: false,
	_key: 'C',
};

/**
 * Alt + Ctrl + Shift + C
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey cd_project_directory_key =
{
	_isAlt: true,
	_isCtrl: false,
	_isShift: true,
	_key: 'C',
};

/**
 * Ctrl + Alt + O
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey open_console_key =
{
	_isAlt: true,
	_isCtrl: true,
	_isShift: false,
	_key: 'O',
};

/**
 * Ctrl + Alt + E
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey close_console_key =
{
	_isAlt: true,
	_isCtrl: true,
	_isShift: false,
	_key: 'E',
};

/**
 * Alt + R
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey auto_run_key =
{
	_isAlt: true,
	_isCtrl: false,
	_isShift: false,
	_key: 'R',
};

/**
 * Ctrl + R
 */
extern(C)
__gshared npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.ShortcutKey repeat_command_key =
{
	_isAlt: false,
	_isCtrl: true,
	_isShift: false,
	_key: 'R',
};
