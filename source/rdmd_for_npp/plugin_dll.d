//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
/**
 * Notepad++ plugin DLL interface
 *
 * See_Also:
 *      http://docs.notepad-plus-plus.org/index.php?title=Plugin_Development
 *
 * License: GPL-2.0 or later
 */
module rdmd_for_npp.plugin_dll;


version (Windows):

private static import core.sys.windows.winuser;
private static import std.path;
private static import std.file;
private static import npp_api.scintilla.Scintilla;
private static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private static import npp_api.pluginfunc.basic_interface;
private static import npp_api.pluginfunc.config_file;
private static import npp_api.pluginfunc.extra_interfece;
private static import npp_api.pluginfunc.menu;
private static import npp_api.pluginfunc.string;
private static import rdmd_for_npp.config;
private static import rdmd_for_npp.console;
private static import rdmd_for_npp.msg;
private static import rdmd_for_npp.project;

pragma(inline, true)
nothrow
void pluginCleanUp()

	in
	{
	}

	do
	{
		static import rdmd_for_npp.console;

		rdmd_for_npp.console.r_console.close();

		try {
			.write_rdmd_config();
		} catch (Exception e) {

		}
	}

nothrow
void pluginSetInfo(ref npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.NppData notpadPlusData)

	do
	{
		static import std.path;
		static import std.file;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.plugin_dll;
		static import rdmd_for_npp.project;

		if (!.plugin_config_file.is_valid_setting) {
			return;
		}

		try {
			plugin_config_file.mkdir(`hello`w);
		} catch (Exception e) {
		}

		if (!auto_settings[search_setting_id!(`first_message`)].value.bool_value) {
			rdmd_for_npp.plugin_dll.trusted_msgbox(rdmd_for_npp.msg.first_message);
		}

		rdmd_for_npp.console.r_console.set_position(cast(int)(auto_settings[search_setting_id!(`previous_x`)].value.int_value), cast(int)(auto_settings[search_setting_id!(`previous_y`)].value.int_value), cast(int)(auto_settings[search_setting_id!(`previous_width`)].value.int_value), cast(int)(auto_settings[search_setting_id!(`previous_height`)].value.int_value));

		wstring current_directory = auto_settings[search_setting_id!(`current_directory`)].value.wstring_value;

		if (current_directory != null) {
			try {
				if (std.path.isValidPath(current_directory) && std.file.exists(current_directory) && std.file.isDir(current_directory)) {
					rdmd_for_npp.console.r_console.init_directory_path(current_directory);
				}
			} catch (Exception e) {
			}
		}

		if (auto_settings[search_setting_id!(`console_path`)].value.wstring_value != null) {
			rdmd_for_npp.console.r_console.default_console_path = auto_settings[search_setting_id!(`console_path`)].value.wstring_value.dup;
		}

		if (auto_settings[search_setting_id!(`project_file`)].value.wstring_value != null) {
			rdmd_for_npp.project.current_project.load(auto_settings[search_setting_id!(`project_file`)].value.wstring_value);
		}

		if (auto_settings[search_setting_id!(`enable_startup_console`)].value.bool_value) {
			rdmd_for_npp.console.r_console.new_console();
		}
	}

nothrow
void write_rdmd_config()

	do
	{
		if (!.plugin_config_file.is_valid_setting) {
			return;
		}

		try {
			if (rdmd_for_npp.console.r_console.current_path.absolute_path.length != 0) {
				.plugin_config_file.write_config(`current_directory`w, rdmd_for_npp.console.r_console.current_path.absolute_path);
			}

			.plugin_config_file.write_config(`previous_x`w, rdmd_for_npp.console.r_console.previous_info.x);
			.plugin_config_file.write_config(`previous_y`w, rdmd_for_npp.console.r_console.previous_info.y);
			.plugin_config_file.write_config(`previous_width`w, rdmd_for_npp.console.r_console.previous_info.width);
			.plugin_config_file.write_config(`previous_height`w, rdmd_for_npp.console.r_console.previous_info.height);

			if (rdmd_for_npp.console.r_console.default_console_path != null) {
				.plugin_config_file.write_config(`console_path`w, rdmd_for_npp.console.r_console.default_console_path);
			} else {
				.plugin_config_file.write_config(`console_path`w, ``w);
			}

			if (rdmd_for_npp.project.current_project.file_name != null) {
				.plugin_config_file.write_config(`project_file`w, rdmd_for_npp.project.current_project.project_path);
			} else {
				.plugin_config_file.write_config(`project_file`w, ``w);
			}

			.plugin_config_file.write_config(`first_message`w, true);
			.plugin_config_file.write_config(`enable_startup_console`w, .menu_index[.search_index!(`Enable_startup_console`)].func_item._init2Check);
		} catch (Exception e) {
		}
	}

static assert(rdmd_for_npp.config.plugin_def.config_info.type != npp_api.pluginfunc.config_file.config_type_t.none);
static assert(rdmd_for_npp.config.plugin_def.config_info.type != npp_api.pluginfunc.config_file.config_type_t.registry);

/*
 * Notepad++ interface functions
 */
mixin npp_api.pluginfunc.extra_interfece.npp_plugin_interface!(rdmd_for_npp.config.plugin_def);
