//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
/**
 * Auto run module
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module rdmd_for_npp.auto_action;


version (Windows):

private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;
private static import std.algorithm;
private static import std.file;
private static import std.path;
private static import std.string;
private static import std.traits;
private static import npp_api.PowerEditor.menuCmdID;
private static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private static import npp_api.pluginfunc.npp_msgs;
private static import npp_api.pluginfunc.lang;
private static import rdmd_for_npp.console;
private static import rdmd_for_npp.dlang_option;
private static import rdmd_for_npp.plugin_dll;
private static import npp_api.pluginfunc.menu;
private static import npp_api.pluginfunc.string;
private static import npp_api.pluginfunc.path;

extern (C)
extern
npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.NppData nppData;

enum action_type
{
	none,
	file,
	build,
	project,
}

nothrow
wstring dmd_path()

	do
	{
		static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
		static import npp_api.pluginfunc.lang;
		static import std.file;
		static import std.path;
		static import std.string;

		wstring dmd_path = npp_api.pluginfunc.lang.lang_path(npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_D);

		return ((std.string.indexOf(dmd_path, '/') != -1) && (!std.file.exists(dmd_path))) ? (std.path.baseName(dmd_path)) : (dmd_path);
	}

nothrow
wstring ldc2_path()

	do
	{
		static import std.file;
		static import std.path;
		static import npp_api.pluginfunc.path;

		wstring drive_name = npp_api.pluginfunc.path.echo_variables(`HOMEDRIVE`w);
		wstring path_temp1 = null;

		try {
			wstring [] ldc_paths =
			[
				`ldc2/bin/ldc2.exe`w,
				`ldc/bin/ldc2.exe`w,
			];

			foreach (wstring ldc_path_temp; ldc_paths) {
				wstring path_temp2 = std.path.buildNormalizedPath(drive_name, ldc_path_temp);

				if (std.file.exists(path_temp2) && std.file.isFile(path_temp2)) {
					path_temp1 = path_temp2.idup;

					break;
				}
			}

			if (path_temp1 != null) {
				return path_temp1;
			}
		} catch (Exception e) {
		}

		return npp_api.pluginfunc.path.search_exe(`ldc2.exe`w);
	}

nothrow
bool is_absolute_dmd_rdmd_path()

	do
	{
		static import std.path;

		return std.path.isAbsolute(.dmd_path());
	}

nothrow
bool is_absolute_ldc2_rdmd_path()

	do
	{
		static import std.path;

		return std.path.isAbsolute(.ldc2_path());
	}

nothrow
wstring dmd_rdmd_path()

	do
	{
		static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
		static import std.file;
		static import std.path;

		wstring temp = .dmd_path();

		return (std.path.isAbsolute(temp)) ? (std.path.buildNormalizedPath(std.path.dirName(temp), `rdmd.exe`w)) : (temp);
	}

nothrow
wstring ldc2_rdmd_path()

	do
	{
		static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
		static import std.file;
		static import std.path;

		wstring temp = .ldc2_path();

		return (std.path.isAbsolute(temp)) ? (std.path.buildNormalizedPath(std.path.dirName(temp), `rdmd.exe`w)) : (temp);
	}

void dmd_command(wstring input_path = null, wstring input_option = null)

	do
	{
		static import npp_api.pluginfunc.menu;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.dlang_option;
		static import rdmd_for_npp.plugin_dll;

		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_rdmd`)].func_item._init2Check) {
			rdmd_for_npp.console.r_console.exec_command(.dmd_rdmd_path(), rdmd_for_npp.dlang_option.dmd_option(input_option), input_path, true);
		} else {
			rdmd_for_npp.console.r_console.exec_command(.dmd_path(), rdmd_for_npp.dlang_option.dmd_option(input_option), input_path, true);
		}
	}

void ldc2_command(wstring input_path = null, wstring input_option = null)

	do
	{
		static import npp_api.pluginfunc.menu;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.dlang_option;
		static import rdmd_for_npp.plugin_dll;

		if ((rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_rdmd`)].func_item._init2Check) && !(npp_api.pluginfunc.menu.is_chid_menu_checked(rdmd_for_npp.plugin_dll.menu_index, rdmd_for_npp.plugin_dll.search_index!(`ldc2 cross compiling`)) && !rdmd_for_npp.dlang_option.is_ldc2_windows_cross_compiling())) {
			rdmd_for_npp.console.r_console.exec_command(.ldc2_rdmd_path(), rdmd_for_npp.dlang_option.ldc_option(input_option), input_path, true);
		} else {
			rdmd_for_npp.console.r_console.exec_command(.ldc2_path(), rdmd_for_npp.dlang_option.ldc_option(input_option), input_path, true);
		}
	}

void dlang_command(wstring input_path = null, wstring input_option = null)

	do
	{
		if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`compiler_ldc2`)].func_item._init2Check) {
			.ldc2_command(input_path, null);
		} else {
			.dmd_command(input_path, null);
		}
	}

/**
 * デフォルトのHelloファイルの内容を返す
 * ToDo:
 */
pure nothrow @safe @nogc @live
string load_default_p_text(wstring ext)

	do
	{
		switch (ext) {
			case `.d`w:
				return import(`hello.d`);

			case `.go`w:
				return import(`hello.go`);

			case `.php`w:
				return import(`hello.php`);

			case `.py`w:
				return import(`hello.py`);

			case `.rb`w:
				return import(`hello.rb`);

			case `.rs`w:
				return import(`hello.rs`);

			default:
				return null;
		}
	}

nothrow
void cd_action(.action_type type)

	do
	{
		static import std.path;
		static import npp_api.pluginfunc.path;
		static import rdmd_for_npp.plugin_dll;
		static import rdmd_for_npp.console;

		switch (type) {
			case .action_type.file:
				if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_cd_at_compile_time`)].func_item._init2Check) {
					rdmd_for_npp.console.r_console.cd(npp_api.pluginfunc.path.get_directory_path(.nppData._nppHandle));
				}

				break;

			default:
				break;
		}
	}

/**
 * ビルドファイルを逆から順に探索する
 *
 * Params:
 *      absolute_path = 有効なフルPATH
 *      search_file_list = 見つけたいビルドファイル名のリスト
 *
 * Returns: フルPATH、見つからなかったらnull
 */
nothrow @safe
wstring search_make(const wchar[] absolute_path, const wstring[] search_file_list)

	in
	{
		static import std.path;

		assert(std.path.isAbsolute(absolute_path));
		assert(search_file_list.length != 0);
	}

	do
	{
		static import std.file;
		static import std.path;

		wstring directory_path = std.path.dirName(absolute_path).idup;
		wstring path_temp;
		wstring add_path;

		foreach_reverse (path_name; std.path.pathSplitter(directory_path)) {
			foreach (file_name; search_file_list) {
				path_temp = std.path.buildNormalizedPath(directory_path, add_path, std.path.baseName(file_name));

				try {
					if (std.file.exists(path_temp) && std.file.isFile(path_temp)) {
						return path_temp.idup;
					}
				} catch (Exception e) {
					//ToDo:
				}
			}

			add_path ~= `../`w;
		}

		return null;
	}

/**
 * send dmd or ldc2 or rdmd command
 */
void dlang_action(rdmd_for_npp.dlang_option.dlang_compiler compiler)

	do
	{
		static import npp_api.pluginfunc.npp_msgs;
		static import npp_api.pluginfunc.path;
		static import npp_api.PowerEditor.menuCmdID;
		static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.dlang_option;
		static import std.file;

		npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType lang_type;
		npp_api.pluginfunc.npp_msgs.send_NPPM_GETCURRENTLANGTYPE(.nppData._nppHandle, lang_type);

		if (lang_type != npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_D) {
			//force dlang style
			npp_api.pluginfunc.npp_msgs.send_NPPM_SETCURRENTLANGTYPE(.nppData._nppHandle, npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_D);
		}

		//Save
		npp_api.pluginfunc.npp_msgs.send_NPPM_MENUCOMMAND(.nppData._nppHandle, npp_api.PowerEditor.menuCmdID.IDM_FILE_SAVE);

		.cd_action(.action_type.file);

		wstring absolute_path = npp_api.pluginfunc.path.get_file_path(.nppData._nppHandle);

		if ((absolute_path.length != 0) && std.file.exists(absolute_path) && std.file.isFile(absolute_path)) {
			switch (compiler) {
				case rdmd_for_npp.dlang_option.dlang_compiler.dmd:
					.dmd_command(absolute_path, null);

					break;

				case rdmd_for_npp.dlang_option.dlang_compiler.ldc2:
					.ldc2_command(absolute_path, null);

					break;

				default:
					assert(false);
			}
		}
	}

/**
 * send dub command
 */
void dub_action(const wchar[] exe_path, const wchar[] argv)

	in
	{
		static import std.path;
		static import rdmd_for_npp.plugin_dll;

		assert(exe_path.length != 0);
	}

	do
	{
		static import npp_api.PowerEditor.menuCmdID;
		static import npp_api.pluginfunc.npp_msgs;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.dlang_option;

		//Save
		npp_api.pluginfunc.npp_msgs.send_NPPM_MENUCOMMAND(.nppData._nppHandle, npp_api.PowerEditor.menuCmdID.IDM_FILE_SAVE);

		rdmd_for_npp.console.r_console.exec_command(exe_path, rdmd_for_npp.dlang_option.dub_option(argv));
	}

bool json_action(wstring input_path)

	do
	{
		static import std.algorithm;
		static import std.path;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.dlang_option;

		if (std.algorithm.cmp(std.path.baseName(input_path), `dub.json`w) == 0) {
			//D dub file
			rdmd_for_npp.console.r_console.cd(input_path);
			rdmd_for_npp.console.r_console.exec_command(npp_api.pluginfunc.lang.lang_path(`.dub`w), rdmd_for_npp.dlang_option.dub_option(``w));

			return true;
		}

		return false;
	}

/**
 * 言語タイプから自動的に実行する
 */
void auto_action(wstring input_path, npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType lang_type)

	do
	{
		static import core.sys.windows.winnt;
		static import core.sys.windows.winuser;
		static import std.file;
		static import std.path;
		static import std.string;
		static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
		static import npp_api.pluginfunc.lang;
		static import rdmd_for_npp.console;
		static import rdmd_for_npp.dlang_option;

		.action_type compile_action = .action_type.none;
		wstring exe_path = null;

		if (.json_action(input_path)) {
			return;
		}

		if (lang_type == npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_D) {
			.cd_action(.action_type.file);
			.dlang_command(input_path, null);

			return;
		}

		wstring extension = std.path.extension(input_path);

		if (npp_api.pluginfunc.lang.is_p_lang(lang_type)) {
			exe_path = npp_api.pluginfunc.lang.lang_path(lang_type);

			if ((std.string.indexOf(exe_path, '/') != -1) && (!std.file.exists(exe_path))) {
				exe_path = std.path.baseName(exe_path);
			}
		} else {
			npp_api.pluginfunc.lang.set_lang(.nppData._nppHandle, extension);

			switch (npp_api.pluginfunc.lang.ext_to_lang(extension)) {
				case npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_D:
					.cd_action(.action_type.file);
					.dlang_command(input_path, null);

					return;

				default:
					exe_path = npp_api.pluginfunc.lang.lang_path(extension);

					if ((std.string.indexOf(exe_path, '/') != -1) && (!std.file.exists(exe_path))) {
						exe_path = std.path.baseName(exe_path);
					}

					switch (extension) {
						case `.go`w:
							rdmd_for_npp.console.r_console.exec_command(exe_path, (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_rdmd`)].func_item._init2Check) ? (`run`w) : (`build`w), input_path, npp_api.pluginfunc.lang.is_extended_path_supported(extension));

							return;

						default:
							break;
					}

					break;
			}
		}

		if (exe_path.length != 0) {
			.cd_action(.action_type.file);
			rdmd_for_npp.console.r_console.exec_command(exe_path, ``w, input_path, npp_api.pluginfunc.lang.is_extended_path_supported(extension));
		}
	}
