//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
/**
 * rdmd console
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module rdmd_for_npp.console;


version (Windows):

private static import core.stdc.string;
private static import core.sys.windows.basetsd;
private static import core.sys.windows.winbase;
private static import core.sys.windows.wincon;
private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;
private static import std.algorithm;
private static import std.file;
private static import std.format;
private static import std.path;
private static import std.string;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private static import npp_api.pluginfunc.menu;
private static import npp_api.pluginfunc.basic_interface;
private static import npp_api.pluginfunc.path;
private static import rdmd_for_npp.plugin_dll;

extern (C)
extern
npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.NppData nppData;

/**
 *
 */
.rdmd_console r_console;

//XP2000
//2047

//XP以降
//8191

/**
 * 検索結果に一致するプロセスIDに終了メッセージを送る
 */
extern (Windows)
nothrow @nogc
core.sys.windows.windef.BOOL CloseProc(core.sys.windows.windef.HWND hWnd, core.sys.windows.windef.LPARAM lParam)

	in
	{
		static import rdmd_for_npp.plugin_dll;

		static assert(rdmd_for_npp.plugin_dll.plugin_name.length != 0);
		static assert((int.max - 1) >= rdmd_for_npp.plugin_dll.plugin_name.length);
		static assert(rdmd_for_npp.plugin_dll.plugin_name[$ - 1] == '\0');
	}

	do
	{
		static import core.stdc.string;
		static import core.sys.windows.basetsd;
		static import core.sys.windows.winbase;
		static import core.sys.windows.windef;
		static import core.sys.windows.winnt;
		static import core.sys.windows.winuser;
		static import rdmd_for_npp.plugin_dll;

		core.sys.windows.windef.DWORD PROCESS_ID;
		core.sys.windows.winbase.GetWindowThreadProcessId(hWnd, &PROCESS_ID);

		enum size_t temp_length = rdmd_for_npp.plugin_dll.plugin_name.length;
		core.sys.windows.winnt.WCHAR[temp_length] buf = '\0';

		if ((PROCESS_ID == ((*(cast(core.sys.windows.winbase.PROCESS_INFORMATION*)(lParam))).dwProcessId)) && (core.sys.windows.winuser.GetWindowTextW(hWnd, &(buf[0]), cast(int)(temp_length)) == (cast(int)(temp_length - 1))) && (core.stdc.string.memcmp(&(buf[0]), &(rdmd_for_npp.plugin_dll.plugin_name[0]), temp_length - 1) == 0)) {
			core.sys.windows.winuser.PostMessageW(hWnd, core.sys.windows.winuser.WM_CLOSE, 0, 0);

			return core.sys.windows.windef.FALSE;
		}

		return core.sys.windows.windef.TRUE;
	}

/**
 * コンソールのイベントハンドラ
 */
extern (Windows)
nothrow
core.sys.windows.windef.BOOL key_handler(core.sys.windows.windef.DWORD handler_type)

	do
	{
		static import core.sys.windows.wincon;
		static import core.sys.windows.windef;
		static import rdmd_for_npp.plugin_dll;

		switch (handler_type) {
			//case core.sys.windows.wincon.CTRL_C_EVENT:
			case core.sys.windows.wincon.CTRL_BREAK_EVENT:
				.r_console.close();

				break;

			case core.sys.windows.wincon.CTRL_CLOSE_EVENT:
			case core.sys.windows.wincon.CTRL_LOGOFF_EVENT:
			case core.sys.windows.wincon.CTRL_SHUTDOWN_EVENT:
				rdmd_for_npp.plugin_dll.pluginCleanUp();

				break;

			default:
				break;
		}

		return core.sys.windows.windef.TRUE;
	}

struct path_info
{
	wstring drive_name = null;
	wstring absolute_path = null;

	invariant
	{
		if (this.absolute_path.length != 0) {
			assert(this.drive_name.length != 0);
		}
	}
}

private struct previous_infomation
{
	/**
	 *
	 */
	wstring console_path;
	wstring command;
	int x;
	int y;
	int width;
	int height;
}

private struct console_window_info
{
	static import core.sys.windows.basetsd;
	static import core.sys.windows.windef;

	core.sys.windows.windef.HWND window;
	core.sys.windows.basetsd.HANDLE input_handle;
	core.sys.windows.basetsd.HANDLE output_handle;
	core.sys.windows.basetsd.HANDLE error_handle;
}

struct rdmd_console
{
	static import core.sys.windows.winbase;
	static import core.sys.windows.wincon;
	static import core.sys.windows.windef;
	static import core.sys.windows.winnt;
	static import core.sys.windows.winuser;
	static import std.algorithm;
	static import std.file;
	static import std.format;
	static import std.path;
	static import std.string;
	static import rdmd_for_npp.plugin_dll;
	static import npp_api.pluginfunc.menu;
	static import npp_api.pluginfunc.path;

private:
	core.sys.windows.winbase.PROCESS_INFORMATION console_process_info;
	core.sys.windows.winbase.STARTUPINFOW console_startup_info;
	.console_window_info console_window;
	bool is_open;
	wstring current_console_path;
	core.sys.windows.wincon.INPUT_RECORD[] commnad_buf;

	/**
	 * mutable
	 */
	core.sys.windows.winnt.WCHAR[] plugin_name;

public:
	/**
	 * mutable
	 */
	wchar[] default_console_path = null;

	.previous_infomation previous_info;
	.path_info current_path;
	bool enable_custom_position;
	bool is_powershell = false;

private:
	enum core.sys.windows.winbase.STARTUPINFOW default_console_startup_info =
	{
		cb: core.sys.windows.winbase.STARTUPINFOW.sizeof,
		lpReserved: core.sys.windows.windef.NULL,
		lpDesktop: core.sys.windows.windef.NULL,
		lpTitle: core.sys.windows.windef.NULL,
		dwX: 0,
		dwY: 0,
		dwXSize: 0,
		dwYSize: 0,
		dwXCountChars: 0,
		dwYCountChars: 0,
		dwFillAttribute: 0,
		dwFlags: 0x00008000,
		wShowWindow: 0,
		cbReserved2: 0,
		lpReserved2: core.sys.windows.windef.NULL,
		hStdInput: core.sys.windows.windef.NULL,
		hStdOutput: core.sys.windows.windef.NULL,
		hStdError: core.sys.windows.windef.NULL,
	};

	/**
	 * 各種変数を初期化する
	 */
	nothrow
	void initialize()

		do
		{
			this.plugin_name = rdmd_for_npp.plugin_dll.plugin_name.dup;
			//this.console_window.window = core.sys.windows.windef.NULL;
			this.console_startup_info = this.default_console_startup_info;
			this.console_startup_info.lpTitle = &(this.plugin_name[0]);

			if (this.default_console_path == null) {
				wstring temp = npp_api.pluginfunc.path.echo_variables(`ComSpec`w);
				this.default_console_path = (temp != null) ? (temp.dup) : (`C:\Windows\System32\cmd.exe`w.dup);
			}

			this.is_powershell = false;
		}

	/**
	 * コンソールを閉じてから再度初期化する
	 */
	nothrow
	void reinitialize()

		do
		{
			this.close();
			this.initialize();
		}

	/**
	 *
	 */
	pragma(inline, true)
	nothrow @nogc
	void set_foreground()

		in
		{
			assert(this.console_window.window != core.sys.windows.windef.NULL);
		}

		do
		{
			core.sys.windows.winuser.AllowSetForegroundWindow(this.console_process_info.dwProcessId);
			int max_width = core.sys.windows.winuser.GetSystemMetrics(core.sys.windows.winuser.SM_CXSCREEN);
			int max_height = core.sys.windows.winuser.GetSystemMetrics(core.sys.windows.winuser.SM_CYSCREEN);
			max_width = (max_width <= 0) ? (int.max) : (max_width);
			max_height = (max_height <= 0) ? (int.max) : (max_height);
			core.sys.windows.windef.UINT nomove_option = (((this.previous_info.x == 0) && (this.previous_info.y == 0)) || ((this.previous_info.x > max_width) || (this.previous_info.y > max_height))) ? (core.sys.windows.winuser.SWP_NOMOVE) : (0);
			core.sys.windows.windef.UINT nosize_option = ((this.previous_info.width == 0) || (this.previous_info.height == 0) || ((this.previous_info.width == 640) && (this.previous_info.height == 480)) || ((this.previous_info.width > max_width) || (this.previous_info.height > max_height))) ? (core.sys.windows.winuser.SWP_NOSIZE) : (0);

			if (this.previous_info.x < 0) {
				this.previous_info.x = 0;
			}

			if (this.previous_info.y < 0) {
				this.previous_info.y = 0;
			}

			core.sys.windows.winuser.SetWindowPos(this.console_window.window, core.sys.windows.winuser.HWND_TOPMOST, this.previous_info.x, this.previous_info.y, this.previous_info.width, this.previous_info.height, core.sys.windows.winuser.SWP_NOACTIVATE|nomove_option|nosize_option);
		}

	/**
	 * 取得したコンソールの標準出力などを設定する
	 */
	pragma(inline, true)
	nothrow @nogc
	void set_std()

		do
		{
			this.console_window.window = core.sys.windows.wincon.GetConsoleWindow();

			if (this.console_window.window != core.sys.windows.windef.NULL) {
				core.sys.windows.wincon.SetConsoleCtrlHandler(&key_handler, core.sys.windows.windef.TRUE);
				core.sys.windows.winuser.RemoveMenu(core.sys.windows.winuser.GetSystemMenu(this.console_window.window, core.sys.windows.windef.FALSE), core.sys.windows.winuser.SC_CLOSE, core.sys.windows.winuser.MF_BYCOMMAND);
				this.set_foreground();
			}

			this.console_window.input_handle = core.sys.windows.winbase.GetStdHandle(core.sys.windows.winbase.STD_INPUT_HANDLE);
			this.console_window.output_handle = core.sys.windows.winbase.GetStdHandle(core.sys.windows.winbase.STD_OUTPUT_HANDLE);
			this.console_window.error_handle = core.sys.windows.winbase.GetStdHandle(core.sys.windows.winbase.STD_ERROR_HANDLE);
		}

	pragma(inline, true)
	pure nothrow @safe @nogc @live
	void set_char(ref core.sys.windows.wincon.INPUT_RECORD command, wchar input_char)

		do
		{
			command.EventType = core.sys.windows.wincon.KEY_EVENT;
			command.Event.KeyEvent.bKeyDown = core.sys.windows.windef.TRUE;
			command.Event.KeyEvent.wRepeatCount = 1;
			command.Event.KeyEvent.UnicodeChar = input_char;
		}

	pragma(inline, true)
	pure nothrow @safe @nogc @live
	int set_position(int input_position, int default_position)

		do
		{
			if (input_position == int.max) {
				return default_position;
			} else {
				return input_position;
			}
		}

	/**
	 * 新しくプロセスを割り当ててコンソールを作成する
	 */
	nothrow
	void internal_new_console(core.sys.windows.winnt.LPWSTR custom_console_path = core.sys.windows.windef.NULL)

		in
		{
		}

		do
		{
			if (this.is_open) {
				this.reinitialize();
			} else {
				this.initialize();
			}

			if (custom_console_path != core.sys.windows.windef.NULL) {
				this.default_console_path = (std.string.fromStringz(custom_console_path)).dup;
			}

			if (std.algorithm.cmp(std.path.baseName!(wchar)(this.default_console_path), `powershell.exe`w) == 0) {
				this.is_powershell = true;
			}

			if (this.default_console_path[$ - 1] != '\0') {
				this.default_console_path ~= '\0';
			}

			core.sys.windows.windef.BOOL result = core.sys.windows.winbase.CreateProcessW
			(
				//実行ファイル
				core.sys.windows.windef.NULL,

				//引数
				&(this.default_console_path[0]),

				//プロセスのセキュリティ
				core.sys.windows.windef.NULL,

				//スレッドのセキュリティ
				core.sys.windows.windef.NULL,

				//ハンドルの継承
				core.sys.windows.windef.FALSE,

				//作成フラグ
				core.sys.windows.winbase.CREATE_UNICODE_ENVIRONMENT | core.sys.windows.winbase.CREATE_NEW_PROCESS_GROUP | core.sys.windows.winbase.CREATE_DEFAULT_ERROR_MODE | core.sys.windows.winbase.CREATE_NEW_CONSOLE,

				//環境ブロック
				core.sys.windows.windef.NULL,

				//カレントディレクトリ
				core.sys.windows.windef.NULL,

				//スタートアップ情報
				&(this.console_startup_info),

				//プロセス情報
				&(this.console_process_info)
			);

			if (result == core.sys.windows.windef.FALSE) {
				rdmd_for_npp.plugin_dll.show_windows_error();

				return;
			}

			bool is_successed = false;

			for (size_t i = 0; i < 20; i++) {
				core.sys.windows.winbase.Sleep(50);

				if (core.sys.windows.wincon.AttachConsole(this.console_process_info.dwProcessId)) {
					is_successed = true;

					break;
				}
			}

			//Clear Error
			core.sys.windows.winbase.SetLastError(0);

			if (!is_successed) {
				rdmd_for_npp.plugin_dll.show_windows_error();

				return;
			}

			this.is_open = true;
			this.set_std();

			if (rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`Enable_msvcEnv.bat`)].func_item._init2Check) {
				this.exec_command(npp_api.pluginfunc.path.search_exe(`msvcEnv.bat`w) ~ ((rdmd_for_npp.plugin_dll.menu_index[rdmd_for_npp.plugin_dll.search_index!(`x64`)].func_item._init2Check) ? (` x64`w) : (``w)));
			}
		}

public:
	~this()

		do
		{
			this.close();
		}

	/**
	 * ToDo:
	 */
	nothrow @nogc
	void restore_console()

		in
		{
			assert(this.console_window.window != core.sys.windows.windef.NULL);
		}

		do
		{
			if (core.sys.windows.winuser.IsIconic(this.console_window.window)) {
				core.sys.windows.winuser.ShowWindow(this.console_window.window, core.sys.windows.winuser.SW_RESTORE);
				core.sys.windows.winuser.AllowSetForegroundWindow(this.console_process_info.dwProcessId);
			}
		}

	pragma(inline, true)
	pure nothrow @safe @nogc @live
	bool is_opened()

		do
		{
			return this.is_open;
		}

	//ToDo:
	void init_directory_path(const wchar[] input_path)

		in
		{
			assert(std.path.isAbsolute(input_path));
		}

		do
		{
			this.current_path.drive_name = std.path.driveName(input_path).idup;
			this.current_path.absolute_path = input_path.idup;
		}

	pragma(inline, true)
	pure nothrow @safe @nogc @live
	void set_position(int x, int y, int width, int height)

		do
		{
			this.previous_info.x = this.set_position(x, 0);
			this.previous_info.y = this.set_position(y, 0);
			this.previous_info.width = this.set_position(width, 640);
			this.previous_info.height = this.set_position(height, 480);
		}

	nothrow
	void cd(const wchar[] input_path)

		in
		{
		}

		do
		{
			if (!std.path.isValidPath(input_path)) {
				return;
			}

			if (!this.is_open) {
				this.internal_new_console();

				if (!this.is_open) {
					return;
				}
			}

			{
				const wchar[] drive_name = std.path.driveName(input_path);

				if (std.file.exists(drive_name)) {
					this.exec_command(drive_name);
					this.current_path.drive_name = drive_name.idup;
				}
			}

			try {
				if (std.file.exists(input_path) && std.file.isDir(input_path)) {
					this.exec_command(`cd "`w ~ input_path ~ `"`w);
					this.current_path.absolute_path = input_path.idup;
				}
			} catch (Exception e) {
				//ToDo:
			}
		}

	/**
	 * 新しくプロセスを割り当ててコンソールを作成する
	 */
	nothrow
	void new_console(core.sys.windows.winnt.LPWSTR custom_console_path = core.sys.windows.windef.NULL)

		in
		{
		}

		do
		{
			this.internal_new_console(custom_console_path);

			if (this.current_path.drive_name.length != 0) {
				this.cd(this.current_path.absolute_path);
			} else {
				this.exec_command(`cd "%HOMEPATH%"`w);
			}
		}

	/**
	 * コンソールを閉じる
	 */
	nothrow @nogc
	void close()

		do
		{
			if (this.is_open) {
				if (this.console_window.window != core.sys.windows.windef.NULL) {
					core.sys.windows.windef.RECT window_rect;
					core.sys.windows.windef.RECT client_rect;
					core.sys.windows.winuser.GetWindowRect(this.console_window.window, &window_rect);
					core.sys.windows.winuser.GetClientRect(this.console_window.window, &client_rect);
					this.previous_info.x = window_rect.left;
					this.previous_info.y = window_rect.top;
					this.previous_info.width = window_rect.right - window_rect.left;
					this.previous_info.height = window_rect.bottom - window_rect.top;
					this.exec_exit_command();
				}

				core.sys.windows.wincon.FreeConsole();
				core.sys.windows.winuser.EnumWindows(&CloseProc, cast(core.sys.windows.windef.LPARAM)(&(this.console_process_info)));
				core.sys.windows.winbase.CloseHandle(this.console_process_info.hProcess);
				core.sys.windows.winbase.CloseHandle(this.console_process_info.hThread);
				this.console_window.input_handle = core.sys.windows.windef.NULL;
				this.console_window.output_handle = core.sys.windows.windef.NULL;
				this.console_window.error_handle = core.sys.windows.windef.NULL;
				this.console_window.window = core.sys.windows.windef.NULL;
				this.console_process_info.hProcess = core.sys.windows.windef.NULL;
				this.console_process_info.hThread = core.sys.windows.windef.NULL;
				this.console_process_info.dwProcessId = 0;
				this.is_open = false;
			}
		}

	/**
	 * コンソールへ直接文字列を送って実行させる形式で実行結果を得る
	 */
	nothrow
	void exec_command(const wchar[] command)

		in
		{
			assert(command.length != 0);
			assert((core.sys.windows.windef.DWORD.max - 2) >= command.length);
		}

		do
		{
			if (!this.is_open) {
				this.new_console();

				if (!this.is_open) {
					return;
				}
			}

			this.restore_console();

			size_t command_length = command.length + 2;
			size_t i = 0;

			if (command_length > this.commnad_buf.length) {
				this.commnad_buf = new core.sys.windows.wincon.INPUT_RECORD[command_length];
			}

			for (; i < command.length; i++) {
				this.set_char(this.commnad_buf[i], command[i]);
			}

			this.set_char(this.commnad_buf[i++], '\r');
			this.set_char(this.commnad_buf[i++], '\n');
			core.sys.windows.windef.DWORD word_count;
			core.sys.windows.wincon.WriteConsoleInputW(this.console_window.input_handle, &(this.commnad_buf[0]), cast(core.sys.windows.windef.DWORD)(command_length), &word_count);
		}

	void exec_command(const wchar[] exe_path, const wchar[] argv)

		in
		{
			assert(exe_path.length != 0);
		}

		do
		{
			if (this.console_process_info.dwProcessId == 0) {
				this.new_console();
			}

			wstring command;

			if (argv.length == 0) {
				command = ((this.is_powershell) ? (`&`w) : (``w)) ~ std.format.format!(`"%s"`w)(exe_path);
			} else {
				command = ((this.is_powershell) ? (`&`w) : (``w)) ~ std.format.format!(`"%s" %s`w)(exe_path, argv);
			}

			this.exec_command(command);
		}

	void exec_command(const wchar[] exe_path, const wchar[] argv, const wchar[] file_path, bool enable_extended_path = false)

		in
		{
			assert(exe_path.length != 0);
			assert(file_path.length != 0);
		}

		do
		{
			if (this.console_process_info.dwProcessId == 0) {
				this.new_console();
			}

			wstring command;

			if (enable_extended_path) {
				if (argv.length == 0) {
					command = ((this.is_powershell) ? (`&`w) : (``w)) ~ std.format.format!(`"%s" "\\?\%s"`w)(exe_path, file_path);
				} else {
					command = ((this.is_powershell) ? (`&`w) : (``w)) ~ std.format.format!(`"%s" %s "\\?\%s"`w)(exe_path, argv, file_path);
				}
			} else {
				if (argv.length == 0) {
					command = ((this.is_powershell) ? (`&`w) : (``w)) ~ std.format.format!(`"%s" "%s"`w)(exe_path, file_path);
				} else {
					command = ((this.is_powershell) ? (`&`w) : (``w)) ~ std.format.format!(`"%s" %s "%s"`w)(exe_path, argv, file_path);
				}
			}

			this.exec_command(command);
		}

	nothrow @nogc
	void exec_exit_command()

		do
		{
			wstring command = "exit\r\n"w;
			core.sys.windows.wincon.INPUT_RECORD[6] input_def;

			for (size_t i = 0; i < command.length; i++) {
				this.set_char(input_def[i], command[i]);
			}

			core.sys.windows.windef.DWORD word_count;
			core.sys.windows.wincon.WriteConsoleInputW(this.console_window.input_handle, &(input_def[0]), cast(core.sys.windows.windef.DWORD)(input_def.length), &word_count);
		}
}
