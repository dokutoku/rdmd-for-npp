//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
/**
 * mixinしたメニュー
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: GPL-2.0 or later
 */
module rdmd_for_npp.auto_menu_commands;


version (Windows):

private static import npp_api.pluginfunc.auto_pFunc;
private static import npp_api.pluginfunc.path;
private static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private import rdmd_for_npp.plugin_dll;

extern (C)
extern
npp_api.PowerEditor.MISC.PluginsManager.PluginInterface.NppData nppData;

mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`cmd`) check_cmd_console;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`powershell_console`) check_powershell_console;
//mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`other file`) check_other_file_console;

mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`compiler_dmd`) check_compiler_dmd;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`compiler_ldc2`) check_compiler_ldc2;
//mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`compiler_gdc`) check_compiler_gdc;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`x32`, `x64`) check_enable_x32;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`x64`, `x32`) check_enable_x64;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable_mscoff`) check_enable_mscoff;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_plain`) check_build_plain;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_debug`) check_build_debug;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_release`) check_build_release;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_release_debug`) check_build_release_debug;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_release_nobounds`) check_build_release_nobounds;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_unittest`) check_build_unittest;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_docs`) check_build_docs;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_ddox`) check_build_ddox;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_profile`) check_build_profile;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_profile_gc`) check_build_profile_gc;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_cov`) check_build_cov;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_unittest_cov`) check_build_unittest_cov;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`build_syntax`) check_build_syntax;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`x32_Windows`) check_x32_windows;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`x64_Windows`) check_x64_windows;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`x64_Linux`) check_x64_linux;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`x64_MacOS`) check_x64_macOS;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`aarch64`) check_aarch64;
//mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`Android_aarch64`) check_android;
//mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`Android_armv7a`) check_android;
mixin npp_api.pluginfunc.auto_pFunc.mixin_group_menu_checked!(`wasm32`) check_wasm32;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable_main`) check_enable_main;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable_betterC`) check_enable_betterC;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable_rdmd`) check_enable_rdmd;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable_force`) check_enable_force_build;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable_run`) check_enable_run;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable_cd_at_compile_time`) check_cd_at_compile;

mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable_startup_console`) check_enable_startup_console;
mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable_msvcEnv.bat`) check_enable_msvcEnv;
//mixin npp_api.pluginfunc.auto_pFunc.mixin_menu_index_change_check!(`Enable option panel`) check_enable_option_panel;

pragma(inline, false)
nothrow
void internal_create_hello(npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType lang_type)

	do
	{
		static import core.sys.windows.winnt;
		static import std.file;
		static import std.utf;
		static import std.path;
		static import npp_api.PowerEditor.menuCmdID;
		static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
		static import npp_api.pluginfunc.npp_msgs;
		static import rdmd_for_npp.plugin_dll;
		static import npp_api.pluginfunc.scintilla_msg;
		static import npp_api.pluginfunc.string;
		static import rdmd_for_npp.auto_action;
		static import npp_api.pluginfunc.lang;

		wstring hello_ext = npp_api.pluginfunc.lang.lang_to_ext!(wstring)(lang_type);

		if (hello_ext.length == 0) {
			return;
		}

		string hello_text = null;
		wstring hello_filename = std.path.setExtension(`hello`w, hello_ext);

		try {
			core.sys.windows.winnt.WCHAR[npp_api.pluginfunc.path.OS_MAX_PATH] input_buf = '\0';

			if (npp_api.pluginfunc.npp_msgs.send_NPPM_GETPLUGINSCONFIGDIR(.nppData._nppHandle, input_buf.length, &(input_buf[0]))) {
				wstring hello_file = std.path.buildPath(npp_api.pluginfunc.string.from_stringz(input_buf), `rdmd\hello\`w , hello_filename);

				if (std.file.exists(hello_file) && std.file.isFile(hello_file)) {
					hello_text = cast(string)(std.file.read(hello_file));
				} else {
					hello_text = rdmd_for_npp.auto_action.load_default_p_text(npp_api.pluginfunc.lang.lang_to_ext!(wstring)(lang_type));
				}
			}
		} catch (Exception e) {
			return;
		}

		hello_text ~= '\0';

		//New file
		npp_api.pluginfunc.npp_msgs.send_NPPM_MENUCOMMAND(.nppData._nppHandle, npp_api.PowerEditor.menuCmdID.IDM_FILE_NEW);

		//Force UTF-8
		try {
			std.utf.validate(hello_text);
			npp_api.pluginfunc.npp_msgs.send_NPPM_MENUCOMMAND(.nppData._nppHandle, npp_api.PowerEditor.menuCmdID.IDM_FORMAT_CONV2_AS_UTF_8);
		} catch (Exception e) {
			return;
		}

		//Force lang style
		npp_api.pluginfunc.npp_msgs.send_NPPM_SETCURRENTLANGTYPE(.nppData._nppHandle, lang_type);

		int currentEdit = -1;
		npp_api.pluginfunc.npp_msgs.send_NPPM_GETCURRENTSCINTILLA(.nppData._nppHandle, currentEdit);

		if (currentEdit == -1) {
			return;
		}

		if (currentEdit == 0) {
			npp_api.pluginfunc.scintilla_msg.send_SCI_SETTEXT(.nppData._scintillaMainHandle, &(hello_text[0]));
		} else {
			npp_api.pluginfunc.scintilla_msg.send_SCI_SETTEXT(.nppData._scintillaSecondHandle, &(hello_text[0]));
		}
	}

pragma(inline, false)
nothrow
void internal_create_hello(wstring hello_ext)

	in
	{
		assert(hello_ext != null);
	}

	do
	{
		static import core.sys.windows.winnt;
		static import std.file;
		static import std.utf;
		static import std.path;
		static import npp_api.PowerEditor.menuCmdID;
		static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
		static import npp_api.pluginfunc.npp_msgs;
		static import rdmd_for_npp.plugin_dll;
		static import npp_api.pluginfunc.scintilla_msg;
		static import npp_api.pluginfunc.string;
		static import rdmd_for_npp.auto_action;
		static import npp_api.pluginfunc.lang;

		string hello_text = null;
		wstring hello_filename = std.path.setExtension(`hello`w, hello_ext);

		try {
			core.sys.windows.winnt.WCHAR[npp_api.pluginfunc.path.OS_MAX_PATH] input_buf = '\0';

			if (npp_api.pluginfunc.npp_msgs.send_NPPM_GETPLUGINSCONFIGDIR(.nppData._nppHandle, input_buf.length, &(input_buf[0]))) {
				wstring hello_file = std.path.buildPath(npp_api.pluginfunc.string.from_stringz(input_buf), `rdmd\hello\`w , hello_filename);

				if (std.file.exists(hello_file) && std.file.isFile(hello_file)) {
					hello_text = cast(string)(std.file.read(hello_file));
				} else {
					hello_text = rdmd_for_npp.auto_action.load_default_p_text(hello_ext);
				}
			}
		} catch (Exception e) {
			return;
		}

		hello_text ~= '\0';

		//New file
		npp_api.pluginfunc.npp_msgs.send_NPPM_MENUCOMMAND(.nppData._nppHandle, npp_api.PowerEditor.menuCmdID.IDM_FILE_NEW);

		//Force UTF-8
		try {
			std.utf.validate(hello_text);
			npp_api.pluginfunc.npp_msgs.send_NPPM_MENUCOMMAND(.nppData._nppHandle, npp_api.PowerEditor.menuCmdID.IDM_FORMAT_CONV2_AS_UTF_8);
		} catch (Exception e) {
			return;
		}

		npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType lang_type = npp_api.pluginfunc.lang.ext_to_lang(hello_ext);

		//Force lang style
		npp_api.pluginfunc.npp_msgs.send_NPPM_SETCURRENTLANGTYPE(.nppData._nppHandle, lang_type);

		int currentEdit = -1;
		npp_api.pluginfunc.npp_msgs.send_NPPM_GETCURRENTSCINTILLA(.nppData._nppHandle, currentEdit);

		if (currentEdit == -1) {
			return;
		}

		if (currentEdit == 0) {
			npp_api.pluginfunc.scintilla_msg.send_SCI_SETTEXT(.nppData._scintillaMainHandle, &(hello_text[0]));
		} else {
			npp_api.pluginfunc.scintilla_msg.send_SCI_SETTEXT(.nppData._scintillaSecondHandle, &(hello_text[0]));
		}
	}

extern (C)
nothrow
void auto_create_hello(npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType lang_type)()

	do
	{
		internal_create_hello(lang_type);
	}

extern (C)
nothrow
void auto_create_hello(wstring hello_ext)()

	do
	{
		internal_create_hello(hello_ext);
	}
