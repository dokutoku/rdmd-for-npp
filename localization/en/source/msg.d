/**
 * plugin config
 */
module rdmd_for_npp.msg;


private static import npp_api.pluginfunc.string;

enum first_message = npp_api.pluginfunc.string.c_wstring!("Thank you for using RDMD for Notepad++. \r\n\r\nThe author of this plug-in has also developed a third-party Notepad ++ API. But the author has a lot of projects and can't afford to keep developing this software. If you want to continue development, consider making a donation."w);
enum LDC_not_found = `ldc2.exe not found.`w;
