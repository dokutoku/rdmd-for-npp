/**
 * plugin config
 */
module rdmd_for_npp.msg;


private static import npp_api.pluginfunc.string;

enum first_message = npp_api.pluginfunc.string.c_wstring!("RDMD for Notepad++を使ってくださってありがとうございます。\r\n\r\nこのプラグインの作者は、サードパーティ製のNotepad++のAPIも開発しています。ですが作者は多数のプロジェクトを抱えており、このソフトを開発し続ける余裕がありません。もしあたなたが開発の継続を希望するなら、寄付をすることを検討してみてください。"w);
enum LDC_not_found = `ldc2.exeが見つかりませんでした。`w;
