/**
 * plugin config
 */
module rdmd_for_npp.config;


version (Windows):

private static import core.sys.windows.windows;
private static import npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs;
private static import npp_api.PowerEditor.MISC.PluginsManager.PluginInterface;
private static import npp_api.pluginfunc.extra_interfece;
private static import npp_api.pluginfunc.auto_pFunc;
private static import npp_api.pluginfunc.string;
public import rdmd_for_npp.common_config;
private import rdmd_for_npp.menu_commands;
private import rdmd_for_npp.auto_menu_commands;

enum npp_api.pluginfunc.extra_interfece.npp_plugin_definition plugin_def =
{
	name: .name,
	version_string: .version_string,
	author: .author,
	config_info: rdmd_for_npp.common_config.plugin_config,
	menu_items:
	[
		{
			id: `New file from current lang`,
			func_item:
			{
				_itemName: `現在のプログラミング言語からHelloを作成`w,
				_pFunc: &create_hello,
				_init2Check: false,
				_pShKey: &create_hello_key,
			},
		},
		{
			id: `New hello file`,
			func_item:
			{
				_itemName: `新しいHelloファイル`w,
				_pFunc: &create_hello,
				_init2Check: false,
				_pShKey: null,
			},
			sub_menu:
			[
				{
					id: `D hello`,
					func_item:
					{
						_itemName: `D`w,
						_pFunc: &auto_create_hello!(npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_D),
					},
				},
				{
					id: `Go hello`,
					func_item:
					{
						_itemName: `Go`w,
						_pFunc: &auto_create_hello!(`.go`w),
					},
				},
				{
					id: `PHP hello`,
					func_item:
					{
						_itemName: `PHP`w,
						_pFunc: &auto_create_hello!(npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_PHP),
					},
				},
				{
					id: `Python hello`,
					func_item:
					{
						_itemName: `Python`w,
						_pFunc: &auto_create_hello!(npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_PYTHON),
					},
				},
				{
					id: `Ruby hello`,
					func_item:
					{
						_itemName: `Ruby`w,
						_pFunc: &auto_create_hello!(npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_RUBY),
					},
				},
				{
					id: `Rust hello`,
					func_item:
					{
						_itemName: `Rust`w,
						_pFunc: &auto_create_hello!(npp_api.PowerEditor.MISC.PluginsManager.Notepad_plus_msgs.LangType.L_RUST),
					},
				},
			],
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `compiler_option`,
			func_item:
			{
				_itemName: `デフォルトのコンパイラー`w,
				_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
				_init2Check: false,
				_pShKey: null,
			},
			sub_menu:
			[
				{
					id: `compiler_dmd`,
					menu_checked_id: `compiler_dmd_checked`,
					func_item:
					{
						_itemName: `dmd`w,
						_pFunc: &check_compiler_dmd.auto_change_check,
						_init2Check: true,
						_pShKey: null,
					},
				},
				{
					id: `compiler_ldc2`,
					menu_checked_id: `compiler_ldc2_checked`,
					func_item:
					{
						_itemName: `ldc2`w,
						_pFunc: &check_compiler_ldc2.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				/+
				{
					id: `compiler_gdc`,
					menu_checked_id: `compiler_gdc_checked`,
					func_item:
					{
						_itemName: `gdc`w,
						_pFunc: &check_compiler_gdc.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				+/
			],
		},
		{
			id: `x32`,
			menu_checked_id: `enable_x32_checked`,
			func_item:
			{
				_itemName: `x32`w,
				_pFunc: &check_enable_x32.auto_change_check,
				_init2Check: rdmd_for_npp.common_config.is_X86,
				_pShKey: null,
			},
		},
		{
			id: `x64`,
			menu_checked_id: `enable_x64_checked`,
			func_item:
			{
				_itemName: `x64`w,
				_pFunc: &check_enable_x64.auto_change_check,
				_init2Check: rdmd_for_npp.common_config.is_X86_64,
				_pShKey: null,
			},
		},
		{
			id: `Enable_mscoff`,
			menu_checked_id: `enable_mscoff_checked`,
			func_item:
			{
				_itemName: `mscoffを有効にする`w,
				_pFunc: &check_enable_mscoff.auto_change_check,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			id: `build_type`,
			func_item:
			{
				_itemName: `ビルドの種類`w,
				_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
				_init2Check: false,
				_pShKey: null,
			},
			sub_menu:
			[
				{
					id: `build_plain`,
					menu_checked_id: `build_plain_checked`,
					func_item:
					{
						_itemName: `plain`w,
						_pFunc: &check_build_plain.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_debug`,
					menu_checked_id: `build_debug_checked`,
					func_item:
					{
						_itemName: `debug`w,
						_pFunc: &check_build_debug.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_release`,
					menu_checked_id: `build_release_checked`,
					func_item:
					{
						_itemName: `release`w,
						_pFunc: &check_build_release.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_release_debug`,
					menu_checked_id: `build_release_debug_checked`,
					func_item:
					{
						_itemName: `release-debug`w,
						_pFunc: &check_build_release_debug.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_release_nobounds`,
					menu_checked_id: `build_release_nobounds_checked`,
					func_item:
					{
						_itemName: `release-nobounds`w,
						_pFunc: &check_build_release_nobounds.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_unittest`,
					menu_checked_id: `build_unittest_checked`,
					func_item:
					{
						_itemName: `unittest`w,
						_pFunc: &check_build_unittest.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_docs`,
					menu_checked_id: `build_docs_checked`,
					func_item:
					{
						_itemName: `docs`w,
						_pFunc: &check_build_docs.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_ddox`,
					menu_checked_id: `build_ddox_checked`,
					func_item:
					{
						_itemName: `ddox`w,
						_pFunc: &check_build_ddox.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_profile`,
					menu_checked_id: `build_profile_checked`,
					func_item:
					{
						_itemName: `profile`w,
						_pFunc: &check_build_profile.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_profile_gc`,
					menu_checked_id: `build_profile_gc_checked`,
					func_item:
					{
						_itemName: `profile-gc`w,
						_pFunc: &check_build_profile_gc.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_cov`,
					menu_checked_id: `build_cov_checked`,
					func_item:
					{
						_itemName: `cov`w,
						_pFunc: &check_build_cov.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_unittest_cov`,
					menu_checked_id: `build_unittest_cov_checked`,
					func_item:
					{
						_itemName: `unittest-cov`w,
						_pFunc: &check_build_unittest_cov.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `build_syntax`,
					menu_checked_id: `build_syntax_checked`,
					func_item:
					{
						_itemName: `syntax`w,
						_pFunc: &check_build_syntax.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
			],
		},
		{
			id: `ldc2 cross compiling`,
			func_item:
			{
				_itemName: `ldc2 クロスコンパイリング`w,
				_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
				_init2Check: false,
				_pShKey: null,
			},
			sub_menu:
			[
				{
					id: `x32_Windows`,
					menu_checked_id: `x32_windows_checked`,
					func_item:
					{
						_itemName: `x32 Windows`w,
						_pFunc: &check_x32_windows.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `x64_Windows`,
					menu_checked_id: `x64_windows_checked`,
					func_item:
					{
						_itemName: `x64 Windows`w,
						_pFunc: &check_x64_windows.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `x64_Linux`,
					menu_checked_id: `x64_linux_checked`,
					func_item:
					{
						_itemName: `x64 Linux`w,
						_pFunc: &check_x64_linux.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `x64_MacOS`,
					menu_checked_id: `x64_macOS_checked`,
					func_item:
					{
						_itemName: `x64 MacOS`w,
						_pFunc: &check_x64_macOS.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `aarch64`,
					menu_checked_id: `aarch64_checked`,
					func_item:
					{
						_itemName: `aarch64`w,
						_pFunc: &check_aarch64.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				/+
				{
					id: `Android_aarch64`,
					menu_checked_id: `android_aarch64_checked`,
					func_item:
					{
						_itemName: `Android_aarch64`w,
						_pFunc: &check_android_aarch64.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				{
					id: `Android_armv7a`,
					menu_checked_id: `android_armv7a_checked`,
					func_item:
					{
						_itemName: `Android_armv7a`w,
						_pFunc: &check_android_armv7a.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
				+/
				{
					id: `wasm32`,
					menu_checked_id: `wasm32_checked`,
					func_item:
					{
						_itemName: `wasm32`w,
						_pFunc: &check_wasm32.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
			],
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `dmd`,
			func_item:
			{
				_itemName: `dmd`w,
				_pFunc: &dmd_run,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			id: `ldc2`,
			func_item:
			{
				_itemName: `ldc2`w,
				_pFunc: &ldc2_run,
				_init2Check: false,
				_pShKey: &ldc_run_key,
			},
		},
/+
		{
			id: `gdc`,
			func_item:
			{
				_itemName: `gdc`w,
				_pFunc: &gdc_run,
				_init2Check: false,
				_pShKey: null,
			},
		},
+/
		{
			id: `Enable_betterC`,
			menu_checked_id: `enable_betterC_checked`,
			func_item:
			{
				_itemName: `-betterC`w,
				_pFunc: &check_enable_betterC.auto_change_check,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			id: `Enable_main`,
			menu_checked_id: `enable_main_checked`,
			func_item:
			{
				_itemName: `--main`w,
				_pFunc: &check_enable_main.auto_change_check,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			id: `Enable_rdmd`,
			menu_checked_id: `enable_rdmd_checked`,
			func_item:
			{
				_itemName: `rdmdを有効にする`w,
				_pFunc: &check_enable_rdmd.auto_change_check,
				_init2Check: true,
				_pShKey: null,
			},
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `dub`,
			func_item:
			{
				_itemName: `dub`w,
				_pFunc: &dub_run,
				_init2Check: false,
				_pShKey: &dub_run_key,
			},
		},
		{
			id: `Enable_force`,
			menu_checked_id: `enable_force_build_checked`,
			func_item:
			{
				_itemName: `--force`w,
				_pFunc: &check_enable_force_build.auto_change_check,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			id: `Enable_run`,
			menu_checked_id: `enable_run_checked`,
			func_item:
			{
				_itemName: `runを有効にする`w,
				_pFunc: &check_enable_run.auto_change_check,
				_init2Check: true,
				_pShKey: null,
			},
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `Open project`,
			func_item:
			{
				_itemName: `プロジェクトを開く`w,
				_pFunc: &open_project,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			id: `Close project`,
			func_item:
			{
				_itemName: `プロジェクトを閉じる`w,
				_pFunc: &close_project,
				_init2Check: true,
				_pShKey: null,
			},
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `cd file directory`,
			func_item:
			{
				_itemName: `このファイルのディレクトリにcdする`w,
				_pFunc: &cd_file_directory,
				_init2Check: false,
				_pShKey: &cd_file_directory_key,
			},
		},
		{
			id: `cd project directory`,
			func_item:
			{
				_itemName: `プロジェクトのディレクトリにcdする`w,
				_pFunc: &cd_project_directory,
				_init2Check: false,
				_pShKey: &cd_project_directory_key,
			},
		},
		{
			id: `Search and cd dub directory from this file`,
			func_item:
			{
				_itemName: `このファイルからDUBファイルを検索してcdする`w,
				_pFunc: &search_dub,
				_init2Check: false,
				_pShKey: &search_dub_key,
			},
		},
		{
			id: `Enable_cd_at_compile_time`,
			func_item:
			{
				_itemName: `コンパイル時、自動的にファイルの場所にcdする`w,
				_pFunc: &check_cd_at_compile.auto_change_check,
				_init2Check: true,
				_pShKey: null,
			},
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `Open file directory`,
			func_item:
			{
				_itemName: `ファイルのディレクトリを開く`w,
				_pFunc: &open_file_directory,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			id: `Open project directory`,
			func_item:
			{
				_itemName: `プロジェクトのディレクトリを開く`w,
				_pFunc: &open_project_directory,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `Auto run`,
			func_item:
			{
				_itemName: `自動実行`w,
				_pFunc: &auto_run,
				_init2Check: false,
				_pShKey: &auto_run_key,
			},
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `Open Console`,
			func_item:
			{
				_itemName: `コンソールを開く`w,
				_pFunc: &open_console,
				_init2Check: false,
				_pShKey: &open_console_key,
			},
		},
		{
			id: `Close/Exit Console`,
			func_item:
			{
				_itemName: `コンソールを閉じる`w,
				_pFunc: &close_console,
				_init2Check: false,
				_pShKey: &close_console_key,
			},
		},
		{
			id: `Console Option`,
			func_item:
			{
				_itemName: `コンソールオプション`w,
				_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
			},
			sub_menu:
			[
				{
					id: `Change Console`,
					func_item:
					{
						_itemName: `コンソールの変更`w,
						_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
						_init2Check: false,
						_pShKey: null,
					},
					sub_menu:
					[
						{
							id: `cmd`,
							menu_checked_id: `cmd_checked`,
							func_item:
							{
								_itemName: `cmd`w,
								_pFunc: &cmd_console,
								_init2Check: true,
								_pShKey: null,
							},
						},
						{
							id: `powershell_console`,
							menu_checked_id: `powershell_checked`,
							func_item:
							{
								_itemName: `PowerShell`w,
								_pFunc: &powershell_console,
								_init2Check: false,
								_pShKey: null,
							},
						},
						/+
						{
							id: `other file`,
							menu_checked_id: `other_file_checked`,
							func_item:
							{
								_itemName: `ファイルの指定`w,
								_pFunc: &change_console,
								_init2Check: false,
								_pShKey: null,
							},
						},
						+/
					],
				},
				{
					id: `Open Console Option`,
					func_item:
					{
						_itemName: `コンソールを開くときの動作`w,
						_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
					},
					sub_menu:
					[
						{
							id: `Enable_msvcEnv.bat`,
							menu_checked_id: `enable_msvcEnv_checked`,
							func_item:
							{
								_itemName: `msvcEnv.batの実行`w,
								_pFunc: &check_enable_msvcEnv.auto_change_check,
								_init2Check: false,
								_pShKey: null,
							},
						},
						/+
						{
							id: `Enable option panel`,
							menu_checked_id: `enable_option_panel_checked`,
							func_item:
							{
								_itemName: `オプションパネルを開く(開発予定)`w,
								_pFunc: &check_enable_option_panel.auto_change_check,
								_init2Check: true,
								_pShKey: null,
							},
						},
						+/
					],
				},
				/+
				{
					id: `コンソールの表示順序`,
					func_item:
					{
						_itemName: `コンソールの表示順序`w,
						_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
					},
					sub_menu:
					[
						{
							id: ``,
							func_item:
							{
								_itemName: `常に手前に表示`w,
								_pFunc: null,
								_init2Check: true,
								_pShKey: null,
							},
						},
						{
							id: ``,
							func_item:
							{
								_itemName: `常に背後に表示`w,
								_pFunc: null,
								_init2Check: false,
								_pShKey: null,
							},
						},
						{
							id: ``,
							func_item:
							{
								_itemName: `指定しない`w,
								_pFunc: null,
								_init2Check: false,
								_pShKey: null,
							},
						},
					],
				},
				+/
			],
		},
		{
			id: `Startup Option`,
			func_item:
			{
				_itemName: `起動時の動作`w,
				_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
			},
			sub_menu:
			[
				{
					id: `Enable_startup_console`,
					menu_checked_id: `enable_startup_console_checked`,
					func_item:
					{
						_itemName: `起動時にコンソールを開く`w,
						_pFunc: &check_enable_startup_console.auto_change_check,
						_init2Check: false,
						_pShKey: null,
					},
				},
			],
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `Open hello world folder`,
			func_item:
			{
				_itemName: `ハローワールドフォルダーを開く`w,
				_pFunc: &open_hello,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			id: `Open config folder`,
			func_item:
			{
				_itemName: `configフォルダを開く`w,
				_pFunc: &open_config_folder,
				_init2Check: false,
				_pShKey: null,
			},
		},
		{
			func_item:
			{
				_pFunc: null,
			},
		},
		{
			id: `Web Sites`,
			func_item:
			{
				_itemName: `ウェブサイト`w,
				_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
			},
			sub_menu:
			[
				{
					id: `this plugin pages`,
					func_item:
					{
						_itemName: `このプラグイン`w,
						_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
					},
					sub_menu:
					[
						{
							id: `plugin top`,
							func_item:
							{
								_itemName: `トップ`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://gitlab.com/dokutoku/rdmd-for-npp`w)),
							},
						},
					],
				},
				{
					id: `NPP API`,
					func_item:
					{
						_itemName: `Npp API`w,
						_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
					},
					sub_menu:
					[
						{
							id: `NPP API Project`,
							func_item:
							{
								_itemName: `NPP API`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://gitlab.com/dokutoku/npp-api`w)),
							},
						},
					],
				},
				{
					id: `dlang pages`,
					func_item:
					{
						_itemName: `D言語`w,
						_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
					},
					sub_menu:
					[
						{
							id: `dlang top page`,
							func_item:
							{
								_itemName: `トップ`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://dlang.org/index.html`w)),
							},
						},
						{
							id: `dlang spec page`,
							func_item:
							{
								_itemName: `言語リファレンス`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://dlang.org/spec/spec.html`w)),
							},
						},
						{
							id: `dlang phobos page`,
							func_item:
							{
								_itemName: `Phobos`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://dlang.org/phobos/index.html`w)),
							},
						},
						{
							id: `dlang API page`,
							func_item:
							{
								_itemName: `APIドキュメント`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://dlang.org/library/index.html`w)),
							},
						},
						{
							id: `dub top page`,
							func_item:
							{
								_itemName: `DUB`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://code.dlang.org/`w)),
							},
						},
						{
							id: `dlang wiki page`,
							func_item:
							{
								_itemName: `D言語 Wiki(英語)`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://wiki.dlang.org/The_D_Programming_Language`w)),
							},
						},
						{
							id: `dlang forum page`,
							func_item:
							{
								_itemName: `D言語フォーラム(英語)`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://forum.dlang.org/`w)),
							},
						},
						{
							id: `dlang download pages`,
							func_item:
							{
								_itemName: `ダウンロードページ`w,
								_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
							},
							sub_menu:
							[
								{
									id: `dmd download page`,
									func_item:
									{
										_itemName: `DMD`w,
										_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://dlang.org/download.html`w)),
									},
								},
								{
									id: `ldc download page`,
									func_item:
									{
										_itemName: `LDC`w,
										_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://github.com/ldc-developers/ldc/releases`w)),
									},
								},
								{
									id: `GCC download pages`,
									func_item:
									{
										_itemName: `GCC`w,
										_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
									},
									sub_menu:
									[
										{
											id: `gcc download page`,
											func_item:
											{
												_itemName: `GCCトップ`w,
												_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://gcc.gnu.org`w)),
											},
										},
										{
											id: `sygwin download page`,
											func_item:
											{
												_itemName: `Cygwin`w,
												_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://cygwin.com/install.html`w)),
											},
										},
										{
											id: `mingw download page`,
											func_item:
											{
												_itemName: `MinGw`w,
												_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://osdn.net/projects/mingw/releases/p15522`w)),
											},
										},
										{
											id: `mingw-w64 download page`,
											func_item:
											{
												_itemName: `mingw-w64`w,
												_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`http://mingw-w64.org/doku.php/download`w)),
											},
										},
									],
								},
								{
									id: `vidusld download page`,
									func_item:
									{
										_itemName: `VisualD`w,
										_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://github.com/dlang/visuald/releases`w)),
									},
								},
							],
						},
					],
				},
				{
					id: `dokutoku pages`,
					func_item:
					{
						_itemName: `作者`w,
						_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
					},
					sub_menu:
					[
						{
							id: `dokutoku project list`,
							func_item:
							{
								_itemName: `プロジェクト一覧`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://gitlab.com/dokutoku`w)),
							},
						},
						{
							id: `dokutoku twitter page`,
							func_item:
							{
								_itemName: `Twitter`w,
								_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://twitter.com/dokutoku3`w)),
							},
						},
						{
							id: `donation`,
							func_item:
							{
								_itemName: `寄付`w,
								_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_dummy_func,
							},
							sub_menu:
							[
								{
									id: `dokutoku donation top page`,
									func_item:
									{
										_itemName: `トップ`w,
										_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://dokutoku.gitlab.io/donation/donation-ja.html`w)),
									},
								},
								{
									id: `dokutoku wishlist page`,
									func_item:
									{
										_itemName: `Amazon ほしいものリスト`w,
										_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://www.amazon.co.jp/hz/wishlist/ls/KMSF3MMFEUFD?type=wishlist&filter=unpurchased&sort=priority&viewType=list`w)),
									},
								},
								{
									id: `dokutoku yggdore page`,
									func_item:
									{
										_itemName: `ユグドア`w,
										_pFunc: &(npp_api.pluginfunc.auto_pFunc.auto_open_uri!(`https://dokutoku.gitlab.io/donation/yggdore_redirect.html`w)),
									},
								},
							],
						},
					],
				},
			],
		},
		{
			id: `About`,
			func_item:
			{
				_itemName: `このプラグインについて`w,
				_pFunc: &npp_api.pluginfunc.auto_pFunc.auto_message_box!(.about_msg, .UTF16_name),
				_init2Check: false,
				_pShKey: null,
			},
		},
	],
};
