[![pipeline status](https://gitlab.com/dokutoku/rdmd-for-npp/badges/master/pipeline.svg)](https://gitlab.com/dokutoku/rdmd-for-npp/commits/master)
# RDMD for Notepad++
A plugin for running RDMD on Notepad++.
By using this plugin, programmers can use not only D language but also various language compilations.

This plugin is a **development version** created by a **novice programming**.
Note that **destructive changes** can be **added in sequence**.

The original text of this document is written in [Japanese](README-ja.md).

![RDMD for Notepad++](rdmd-for-npp.gif)

## Characteristic
### Fast startup
This only benefits from Notepad++. But this is very important. You can start compiling your program as soon as you start the editor.

### Automatic execution
You can compile the currently open file with ALT + R.

The programming language is automatically inferred from the information in the file.

### Automatic search for DUB files
Automatically search for DUB files from the currently open file with ALT + C.

### Beginner friendly
For those who use D language for the first time, various ideas have been devised.

- Multilingual support (Japanese, English)
- Menu of frequently used D language options
- Menu of D language related website
- Custom Hello file

## Donation
The project is soliciting donations to continue development.
Please refer to the following site for details.

https://dokutoku.gitlab.io/donation/donation-en.html

## Project status
I spend a lot of time [NPP API](https://gitlab.com/dokutoku/npp-api) and [RDMD for Notepadd++](https://gitlab.com/dokutoku/rdmd-for-npp) Spent on development. And I was able to implement the functions I wanted.

So if there is no donation, we will stop development.
