[![pipeline status](https://gitlab.com/dokutoku/rdmd-for-npp/badges/master/pipeline.svg)](https://gitlab.com/dokutoku/rdmd-for-npp/commits/master)
# RDMD for Notepad++
Notepad++上でRDMDを実行するためのプラグイン。
このプラグインを利用することによって、プログラマはD言語だけでなくさまざまな言語のコンパイルを利用できるようになります。

このプラグインは**プログラミング初心者**が製作した**開発版**です。
**破壊的変更**が**次々に加えられる**可能性があることに注意してください。

この文章の原文は[日本語](README-ja.md)で書かれています。

![RDMD for Notepad++](rdmd-for-npp.gif)

## 特徴
### 高速な起動
これはNotepad++の恩恵を受けているだけに過ぎません。ですがこれはとても重要なことです。エディタを起動してすぐにプログラムをコンパイルを開始することができます。

### 自動実行
ALT+Rで現在開いているファイルをコンパイルすることができます。

プログラミング言語はファイルの情報から自動推察されます。

### DUBファイルの自動検索
ALT+Cで現在開いているファイルからDUBファイルを自動検索します。

### 初心者に優しい
D言語を始めて使う人のために以下のにさまざまな工夫を凝らしています。

- 多言語対応(日本語、英語)
- よく使うD言語オプションのメニュー
- D言語関連ウェブサイトのメニュー
- カスタムHelloファイル

## 寄付
このプロジェクトでは開発継続のための寄付を募っています。
詳細は以下のサイトを参考にしてください。

https://dokutoku.gitlab.io/donation/donation-ja.html

## プロジェクトのステータス
私は非常に多くの時間を[NPP API](https://gitlab.com/dokutoku/npp-api)と[RDMD for Notepadd++](https://gitlab.com/dokutoku/rdmd-for-npp)の開発に費やしました。そして自分が欲しかった機能を実装することができました。

なので寄付がなければこのまま開発を停止する予定です。
