private static import std.file;
private static import std.path;
private static import std.zip;
private static import npp_api.pluginfunc.export_info;

enum npp_api.pluginfunc.export_info.npp_plugin_info_item common_en_info =
{
	display_name: `RDMD for Notepad++(English)`,
	version_: `0.1.0.2`,
	description: `Plugin for running rdmd with Notepad++`,
	author: `dokutoku`,
	homepage: `https://gitlab.com/dokutoku/rdmd-for-npp`,
};

enum npp_api.pluginfunc.export_info.npp_plugin_info_item common_ja_info =
{
	display_name: `RDMD for Notepad++(Japanese)`,
	version_: `0.1.0.2`,
	description: `Plugin for running rdmd with Notepad++`,
	author: `dokutoku`,
	homepage: `https://gitlab.com/dokutoku/rdmd-for-npp`,
};

void compress_zip(string output_name, string input_file_name)

	do
	{
		static import std.file;
		static import std.path;
		static import std.zip;

		string name = std.path.baseName(input_file_name);

		std.zip.ArchiveMember am = new std.zip.ArchiveMember();
		am.name = name;
		am.expandedData(cast(ubyte[])(std.file.read(input_file_name)));
		std.zip.ZipArchive zip = new std.zip.ZipArchive();
		zip.addMember(am);
		std.file.write(output_name, zip.build());
	}

void main()

	do
	{
		static import std.file;

		npp_api.pluginfunc.export_info.npp_plugin_info_item en_x86_item = .common_en_info;
		npp_api.pluginfunc.export_info.npp_plugin_info_item en_x64_item = .common_en_info;
		npp_api.pluginfunc.export_info.npp_plugin_info_item ja_x86_item = .common_ja_info;
		npp_api.pluginfunc.export_info.npp_plugin_info_item ja_x64_item = .common_ja_info;

		en_x86_item.folder_name = `rdmd-en-x86`;
		.compress_zip(`./rdmd-en-x86.zip`, `./../localization/en/plugins/rdmd-en-x86/rdmd-en-x86.dll`);
		en_x86_item.update_id(`./rdmd-en-x86.zip`);
		en_x86_item.repository = ``;

		en_x64_item.folder_name = `rdmd-en-x64`;
		.compress_zip(`./rdmd-en-x64.zip`, `./../localization/en/plugins/rdmd-en-x64/rdmd-en-x64.dll`);
		en_x64_item.update_id(`./rdmd-en-x64.zip`);
		en_x64_item.repository = ``;

		ja_x86_item.folder_name = `rdmd-ja-x86`;
		.compress_zip(`./rdmd-ja-x86.zip`, `./../localization/ja/plugins/rdmd-ja-x86/rdmd-ja-x86.dll`);
		ja_x86_item.update_id(`./rdmd-ja-x86.zip`);
		ja_x86_item.repository = ``;

		ja_x64_item.folder_name = `rdmd-ja-x64`;
		.compress_zip(`./rdmd-ja-x64.zip`, `./../localization/ja/plugins/rdmd-ja-x64/rdmd-ja-x64.dll`);
		ja_x64_item.update_id(`./rdmd-ja-x64.zip`);
		ja_x64_item.repository = ``;

		std.file.write(`./pre_pl.x86.json`, npp_api.pluginfunc.export_info.create_plugin_list_json(en_x86_item.export_json(), ja_x86_item.export_json()));
		std.file.write(`./pre_pl.x64.json`, npp_api.pluginfunc.export_info.create_plugin_list_json(en_x64_item.export_json(), ja_x64_item.export_json()));
	}
